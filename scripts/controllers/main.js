/**
 * @license
 * Copyright Dan Munro and Jack Beckitt-Marshall
 * Released under Expat license <https://directory.fsf.org/wiki/License:Expat>
 */

/**
 * @fileoverview
 * Hauptmodul des Programms - es lädt und prozessiert Bilder, um PBN zu machen!
 */
import each from '../async/lib/each.js'

'use strict'
/**
 * @ngdoc function
 * @name pbnApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the pbnApp
 */

angular.module('pbnApp').controller('MainCtrl', function ($scope) {
  $scope.step = 'load'
  $scope.view = ''
  $scope.status = ''
  $scope.loaderStyle = {
    border: '4px dashed #777777'
  }
  $scope.palette = []
  $scope.colorInfoVisible = false
  $scope.nColors = 6

  // Globals für paralleles Rechnen.
  const outlineMat = []
  const smoothSimp = []
  const covered = []

  /**
   * Kontrolliert was passiert, wenn das Bild hochgeladen wird, wie der Beginn
   * der "PBNIFY-Prozess."
   * @param imgSrc {String} URL für das Bild.
   */
  $scope.imageLoaded = function (imgSrc) {
    const img = new Image()
    img.src = imgSrc
    img.onload = function () {
      const c = document.getElementById('img-canvas')
      // c.width = 800
      c.width = document.getElementById('widthSlider')
        .value
      $scope.nColors = document.getElementById(
        'levelSlider').value
      const scale = c.width / img.naturalWidth
      c.height = img.naturalHeight * scale
      document.getElementById('canvases').style.height = c
        .height + 20 + 'px'
      $scope.c = c
      $scope.ctx = c.getContext('2d')
      $scope.ctx.drawImage(img, 0, 0, c.width, c.height)
      $scope.step = 'select'
      $scope.$apply()
      $scope.pbnify()
    }
  }

  /**
   * Fügt eine Farbe für der Bild hinzu.
   * @param {color} color
   */
  $scope.addColor = function (color) {
    $scope.palette.push(color)
  }

  /**
   * Löscht eine Farbe aus der Palette.
   * @param {color} color
   */
  $scope.removeColor = function (color) {
    _.pull($scope.palette, color)
  }

  /**
   * Für eine Farbe, finden die nächste Farbe in der Palette mit quadratische
   * Entfernung.
   * @param {Color[]} palette Die Palette der Farben
   * @param {Color} col Farbe, sodass die nächste Farbe in der Palette gefunden
   * werden kann.
   */
  const getNearest = function (palette, col) {
    let nearest
    let nearestDistsq = 1000000
    for (let i = 0; i < palette.length; i += 1) {
      const pcol = palette[i]
      const distsq = Math.pow(pcol.r - col.r, 2) +
        Math.pow(pcol.g - col.g, 2) +
        Math.pow(pcol.b - col.b, 2)
      if (distsq < nearestDistsq) {
        nearest = i
        nearestDistsq = distsq
      }
    }
    return nearest
  }

  /**
   * Mit Bilddaten konvertieren das Bild im einfacher Format, mit jeder Pixel
   * als nächste Farbe im Palette repräsentiert wird.
   * @param {Array} imgData Bilddaten als 2D-Array, aber jeder Pixel ist 4
   * Elemente (R, G, B, A).
   * @param {Array} palette Palette für Farbfinden.
   */
  const imageDataToSimpMat = function (imgData, palette) {
    const mat = []
    for (let i = 0; i < imgData.height; i++) {
      mat[i] = new Array(imgData.width)
    }
    for (let i = 0; i < imgData.data.length; i += 4) {
      const nearestI = getNearest(palette, {
        r: imgData.data[i],
        g: imgData.data[i + 1],
        b: imgData.data[i + 2]
      })
      const x = (i / 4) % imgData.width
      const y = Math.floor((i / 4) / imgData.width)
      mat[y][x] = nearestI
    }
    return mat
  }

  /**
   * Konvertiert eine Mat in Bilddaten, sodass es speichern kann.
   * @param {*} mat Mat zu konvertieren
   * @param {*} palette Palette
   * @param {*} context Webseite-Kontext
   */

  const matToImageData = function (mat, palette, context) {
    const imgData = context.createImageData(mat[0].length, mat
      .length)
    for (let y = 0; y < mat.length; y++) {
      for (let x = 0; x < mat[0].length; x++) {
        const i = (y * mat[0].length + x) * 4
        const col = palette[mat[y][x]]
        imgData.data[i] = col.r
        imgData.data[i + 1] = col.g
        imgData.data[i + 2] = col.b
        imgData.data[i + 3] = 255
      }
    }
    return imgData
  }

  /**
   * Zeigt der neuen Bild von PBN-Prozess.
   * @param {Array} matSmooth Geglätterte PBN-Bild.
   * @param {Array} matLine
   * @param {Array} labelLocs
   */
  const displayResults = function (matSmooth, matLine, labelLocs) {
    const c2 = document.getElementById('filled-canvas')
    c2.width = $scope.c.width
    c2.height = $scope.c.height

    // draw filled
    const ctx2 = c2.getContext('2d')
    const imgData = matToImageData(matSmooth, $scope.palette,
      ctx2)
    ctx2.putImageData(imgData, 0, 0)

    const c3 = document.getElementById('outline-canvas')
    c3.width = c2.width
    c3.height = c2.height

    // draw outlines
    // gray value was 191, changed to 150.
    const bw = [{
      r: 255,
      g: 255,
      b: 255
    },
    {
      r: 150,
      g: 150,
      b: 150
    }
    ]
    const ctx3 = c3.getContext('2d')
    const imgDataLine = matToImageData(matLine, bw, ctx3)
    ctx3.putImageData(imgDataLine, 0, 0)

    // draw numbers
    ctx3.font = '12px Georgia'
    ctx3.fillStyle = 'rgb(150, 150, 150)'
    labelLocs.forEach(function (val) {
      ctx3.fillText(
        val.value + 1,
        val.x - 3,
        val.y + 4
      )
    })
    // for (let i = 0; i < labelLocs.length; i++) {
    //       ctx3.fillText(
    //         labelLocs[i].value + 1,
    //         labelLocs[i].x - 3,
    //         labelLocs[i].y + 4
    //       )
    //     }

    $scope.c2 = c2
    $scope.c3 = c3
  }

  const rgbToHex = function (r, g, b) {
    return '#' + ((1 << 24) + (r << 16) + (g << 8) + b)
      .toString(16).slice(1)
  }

  const rgbToCmyk = function (r, g, b) {
    let c
    let y
    let m
    const k = 1 - Math.max(r / 255, g / 255, b / 255)
    if (k === 1) {
      c = 0
      m = 0
      y = 0
    } else {
      c = (1 - r / 255 - k) / (1 - k)
      m = (1 - g / 255 - k) / (1 - k)
      y = (1 - b / 255 - k) / (1 - k)
    }

    return {
      c: Math.round(c * 100),
      m: Math.round(m * 100),
      y: Math.round(y * 100),
      k: Math.round(k * 100)
    }
  }

  const rgbToHsl = function (r, g, b) {
    r = r / 255
    g = g / 255
    b = b / 255

    const M = Math.max(r, g, b)
    const m = Math.min(r, g, b)

    let h
    if (M === m) {
      h = 0
    } else if (M === r) {
      h = (60 * (g - b)) / (M - m)
    } else if (M === g) {
      h = (60 * (b - r)) / (M - m) + 120
    } else {
      h = (60 * (r - g)) / (M - m) + 240
    }

    const l = (M + m) / 2
    let s
    if (l === 0 || l === 1) {
      s = 0 // So it isn't NaN for black or white.
    } else {
      s = (M - m) / (1 - Math.abs(2 * l - 1))
    }

    return {
      h: ((Math.round(h) % 360) + 360) %
        360, // js modulo isn't always positive
      s: Math.round(s * 100),
      l: Math.round(l * 100)
    }
  }

  const rgbToHsv = function (r, g, b) {
    r = r / 255
    g = g / 255
    b = b / 255

    const M = Math.max(r, g, b)
    const m = Math.min(r, g, b)

    let h
    if (M === m) {
      h = 0
    } else if (M === r) {
      h = (60 * (g - b)) / (M - m)
    } else if (M === g) {
      h = (60 * (b - r)) / (M - m) + 120
    } else {
      h = (60 * (r - g)) / (M - m) + 240
    }

    let s
    if (M === 0) {
      s = 0 // So it isn't NaN for black.
    } else {
      s = (M - m) / M
    }

    return {
      h: ((Math.round(h) % 360) + 360) % 360,
      s: Math.round(s * 100),
      v: Math.round(M * 100)
    }
  }

  const getColorInfo = function (palette) {
    for (let i = 0; i < palette.length; i++) {
      const col = palette[i]
      col.hex = rgbToHex(col.r, col.g, col.b)
      col.cmyk = rgbToCmyk(col.r, col.g, col.b)
      col.hsl = rgbToHsl(col.r, col.g, col.b)
      col.hsv = rgbToHsv(col.r, col.g, col.b)
    }
  }

  function SmoothWorkPackage () {
    this.section = []
    this.x = 0
    this.y = 0
    this.result = []
  }

  // Defines a "work package" that can be sent to the worker (in this case for
  // the outline).
  function OutlineWorkPackage () {
    this.section = []
    this.x = 0
    this.y = 0
    this.result = 0
  }

  function LabelLocWorkPackage () {
    this.section = []
    this.x = 0
    this.y = 0
    this.result = 0
  }

    /**
   * Callback-Function für Outline - platziert die richtigen Pixel im richtigen
   * Ort.
   * @param {*} event 
   */
  function outlineCallback (event) {
    const wp = event
    const pixel = wp.result
    outlineMat[wp.y][wp.x] = pixel
  }

  /**
   * Callback-Function für Smooth - platziert die richtigen Pixel im richtigen
   * Ort.
   * @param {*} event 
   */
  function smoothCallback (event) {
    const wp = event
    const pixel = wp.result
    smoothSimp[wp.y][wp.x] = pixel
  }

  /**
   * Funktion, die sucht, ob Nachbarpixels gleich sind.
   * @param {*} mat Matrix
   * @param {*} x X-Position
   * @param {*} y Y-Position.
   */
  function neighborsSame (mat, x, y) {
    if (mat.length === 0) return false
    const width = mat[0].length
    const height = mat.length
    const val = mat[y][x]
    // let xRel = [-1, 0, 1, -1, 0, 1, -1, 0, 1]
    // let yRel = [-1, -1, -1, 0, 0, 0, 1, 1, 1]
    // let xRel = [0, -1, 1, 0]
    // let yRel = [-1, 0, 0, 1]
    const xRel = [1, 0]
    const yRel = [0, 1]
    for (let i = 0; i < xRel.length; i++) {
      const xx = x + xRel[i]
      const yy = y + yRel[i]
      if (xx >= 0 && xx < width && yy >= 0 && yy < height) {
        if (mat[yy][xx] !== val) {
          return false
        }
      }
    }
    return true
  };

  /**
   * Produziert eine Bildkontur für die Regionen.
   * @param {*} mat Matrix, um die Kontur zu produzieren.
   */
  const outline = function (mat) {
    const width = mat[0].length
    const height = mat.length
    for (let i = 0; i < height; i++) {
      outlineMat[i] = new Array(width)
    }
    const workPackages = []
    for (let y = 0; y < height; y++) {
      for (let x = 0; x < width; x++) {
        const wp = new OutlineWorkPackage()
        wp.section = mat.slice(y - 1, y + 2).map(i => i
          .slice(x - 1, x + 2))
        wp.x = x
        wp.y = y
        workPackages.push(wp)
      }
    }
    each(workPackages, function (wp, callback) {
      wp.result = neighborsSame(wp.section, 1, 1)
        ? 0 : 1
      outlineCallback(wp)
    })
    return outlineMat
  }

  $scope.pbnify = function () {
    $scope.step = 'process'
    const width = $scope.c.width
    const height = $scope.c.height
    const imgData = $scope.ctx.getImageData(0, 0, width, height)

    $scope.status = 'Farben finden'
    kMeansPalette(imgData, $scope.nColors)

    const mat = imageDataToSimpMat(imgData, $scope.palette)
    $scope.status = 'Bild glätten'

    const smoothed = smooth(mat)
    $scope.status = 'Labelle bekommen'
    const labelLocs = getLabelLocs(smoothed)

    $scope.status = 'Outline zeichnen'
    const matLine = outline(smoothed)

    displayResults(smoothed, matLine, labelLocs)
    getColorInfo($scope.palette) // adds hex and CMYK values for display
    $scope.step = 'result'
    $scope.view = 'filled'
  }

  $scope.newImage = function () {
    $scope.palette = []
    document.getElementById('canvases').style.height = '0px'
    $scope.step = 'load'
  }

  $scope.recolor = function () {
    $scope.step = 'select'
  }

  $scope.clearPalette = function () {
    $scope.palette = []
  }

  $scope.showColorInfo = function () {
    $scope.colorInfoVisible = true
  }

  $scope.hideColorInfo = function () {
    $scope.colorInfoVisible = false
  }

  $scope.viewFilled = function () {
    $scope.view = 'filled'
  }

  $scope.viewOutline = function () {
    $scope.view = 'outline'
  }

  $scope.saveFilled = function () {
    const win = window.open()
    win.document.write(
      '<html><head><title>PBN filled</title></head><body><img src="' +
      $scope.c2.toDataURL() +
      '"></body></html>'
    )
    // win.print()
  }

  $scope.saveOutline = function () {
    const win = window.open()
    win.document.write(
      '<html><head><title>PBN outline</title></head><body><img src="' +
      $scope.c3.toDataURL() +
      '"></body></html>'
    )
    // win.print()
  }

  $scope.savePalette = function () {
    const canvas = document.createElement('canvas')
    canvas.width = 80 * Math.min($scope.palette.length, 10)
    canvas.height = 80 * (Math.floor(($scope.palette.length -
      1) / 10) + 1)
    const ctx = canvas.getContext('2d')
    ctx.fillStyle = '#ffffff'
    ctx.fillRect(0, 0, canvas.width, canvas.height)
    ctx.strokeStyle = '#000000'
    for (let i = 0; i < $scope.palette.length; i++) {
      const col = $scope.palette[i]
      ctx.fillStyle = 'rgba(' + col.r + ', ' + col.g + ', ' +
        col.b + ', 255)'
      const x = 80 * (i % 10)
      const y = 80 * Math.floor(i / 10)
      ctx.fillRect(x + 10, y + 10, 60, 60)
      ctx.fillStyle = '#ffffff'
      ctx.fillRect(x + 10, y + 10, 20, 20)
      ctx.font = '16px sans-serif'
      ctx.fillStyle = '#000000'
      ctx.textAlign = 'center'
      ctx.fillText(i + 1, x + 20, y + 26)
      ctx.strokeRect(x + 10, y + 10, 60, 60)
    }

    const win = window.open()
    win.document.write(
      '<html><head><title>PBN palette</title></head><body><img src="' +
      canvas.toDataURL() +
      '"></body></html>'
    )
    // win.print()
  }

  /* kmeansPalette - produziert eine Palette automatisch mit K-Means Clustering
   */
  const kMeansPalette = function (imgData, nColors) {
    const colors = []
    // Produziert eine Feld, mit jeder Farbe als (R, G, B).
    for (let i = 0; i < imgData.data.length; i += 4) {
      colors.push([imgData.data[i], imgData.data[i + 1],
        imgData.data[i + 2]
      ])
    }
    const km = kmeans(colors, Math.round(nColors))
    let point
    $scope.clearPalette()
    // F�r jeder Massenmittenpnukt produzieren eine RGB-Farbe.
    for (point of km.centroids) {
      const pcol = {
        r: Math.round(point.centroid[0]),
        g: Math.round(point.centroid[1]),
        b: Math.round(point.centroid[2])
      }
      $scope.palette.push(pcol)
    }
  }

  /**
   * Hilfsfunktion für Bildglätten
   * @param {*} mat Matrix für glätten.
   * @param {*} x Pixel-X
   * @param {*} y Pixel-Y
   * @param {*} range Pixel-Bereich für diese X und Y.
   */
  function getVicinVals (mat, x, y,
    range) { // range is how many pixels on each side to get
    if (mat.length === 0) {
      return [0]
    }
    if (mat.length < 8) {
      x = 0
      y = 0
    }
    const width = mat[0].length
    const height = mat.length
    const vicinVals = []
    for (let xx = x - range; xx <= x + range; xx++) {
      for (let yy = y - range; yy <= y + range; yy++) {
        if (xx >= 0 && xx < width && yy >= 0 && yy < height) {
          vicinVals.push(mat[yy][xx])
        }
      }
    }
    if (!isNaN(vicinVals)) {
      return 0
    }
    return vicinVals
  };

  /**
   * Bekommt die Lagen für der MnZ-Etiketten.
   * @param {*} mat Bild-Matrix, sodass die Lagen gefunden werden.
   */
  const getLabelLocs = function (mat) {
    const workPackages = []
    const width = mat[0].length
    const height = mat.length
    // Erstellt ein Array, das die Abdeckung jedes Pixels anzeigt.
    for (let i = 0; i < height; i++) {
      covered[i] = new Array(width)
      _.fill(covered[i], false)
    }
    const labelLocs = []
    for (let y = 0; y < height; y++) {
      for (let x = 0; x < width; x++) {
        const wp = new LabelLocWorkPackage()
        wp.x = x
        wp.y = y
        wp.mat = mat
        workPackages.push(wp)
      }
    }
    each(workPackages, function (wp, callback) {
      // Füllt der covered-Array.
      if (covered[wp.y][wp.x] === false) {
        const region = {
          value: wp.mat[wp.y][wp.x],
          x: [],
          y: []
        }
        const value = wp.mat[wp.y][wp.x]
        const queue = [
          [wp.x, wp.y]
        ]
        // Bekommt der richtigen Region.
        while (queue.length > 0) {
          const coord = queue.shift()
          if (covered[coord[1]][coord[0]] ===
            false && mat[coord[1]][coord[0]] ===
            value) {
            region.x.push(coord[0])
            region.y.push(coord[1])
            covered[coord[1]][coord[0]] = true
            if (coord[0] > 0) {
              queue.push([coord[0] - 1, coord[
                1]])
            }
            if (coord[0] < mat[0].length - 1) {
              queue.push([coord[0] + 1, coord[
                1]])
            }
            if (coord[1] > 0) {
              queue.push([coord[0], coord[1] -
                1
              ])
            }
            if (coord[1] < mat.length - 1) {
              queue.push([coord[0], coord[1] +
                1
              ])
            }
          }
        }
        for (let i = 0; i < region.x.length; i++) {
          covered[region.y[i]][region.x[i]] =
            true
        }
        // Wenn die Länge der Region > 100, füge eine Etikette hinzu, aber wenn
        // nicht, lösche es.
        if (region.x.length > 100) {
          let bestI = 0
          let best = 0
          const sameCount = function (mat, x, y,
            incX, incY) {
            const value = mat[y][x]
            let count = -1
            while (x >= 0 && x < mat[0]
              .length && y >= 0 && y < mat
              .length && mat[y][x] ===
              value) {
              count++
              x += incX
              y += incY
            }
            return count
          }
          for (let i = 0; i < region.x.length; i++) {
            const goodness = sameCount(wp.mat,
              region.x[i], region.y[i], -1, 0) *
              sameCount(mat, region.x[i],
                region.y[i], 1, 0) *
              sameCount(mat, region.x[i],
                region.y[i], 0, -1) *
              sameCount(mat, region.x[i],
                region.y[i], 0, 1)
            if (goodness > best) {
              best = goodness
              bestI = i
            }
          }
          labelLocs.push({
            value: region.value,
            x: region.x[bestI],
            y: region.y[bestI]
          })
        } else {
          let newValue
          if (region.y[0] > 0) {
            newValue = mat[region.y[0] - 1][
              region.x[0]
            ] // assumes first pixel in list is topmost then leftmost of region.
          } else {
            const x = region.x[0]
            let y = region.y[0]
            while (wp.mat[y][x] === region
              .value) {
              y++
            }
            newValue = mat[y][x]
            // let newValue = getBelowValue(mat, region)
          }
          for (let i = 0; i < region.x
            .length; i++) {
            wp.mat[region.y[i]][region.x[i]] =
              newValue
          }
        }
      }
    })
    return labelLocs
  }

  /**
   * Glättert der Bild mit eine parallele Prozess.
   * @param {*} mat Bild zu glättern.
   */
  const smooth = function (mat) {
    const workPackages = []
    const width = mat[0].length
    const height = mat.length
    for (let i = 0; i < height; i++) {
      smoothSimp[i] = new Array(width)
    }
    for (let y = 0; y < height; y++) {
      for (let x = 0; x < width; x++) {
        const wp = new SmoothWorkPackage()
        wp.section = mat
        // wp.section = mat.slice(y-4, y+5).map(i => i.slice(x-4, x+5))
        // console.log(wp.section)
        // wp.mat = mat
        wp.x = x
        wp.y = y
        workPackages.push(wp)
      }
    }

    each(workPackages, function (wp, callback) {
      const vicinVals = getVicinVals(wp.section, wp.x,
        wp.y, 4)
      wp.result = Number(_.chain(vicinVals).countBy()
        .toPairs().maxBy(_.last).head().value())
      smoothCallback(wp)
    })
    return smoothSimp
  }
})
