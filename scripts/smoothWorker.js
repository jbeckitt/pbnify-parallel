importScripts("lodash.js")
importScripts("run-parallel-bundle.js")

function getVicinVals(mat, x, y, range) {	// range is how many pixels on each side to get
    var width = mat[0].length;
    var height = mat.length;
    var vicinVals = [];
    for (var xx = x - range; xx <= x + range; xx++) {
	for (var yy = y - range; yy <= y + range; yy++) {
	    if (xx >= 0 && xx < width && yy >= 0 && yy < height) {
		vicinVals.push(mat[yy][xx]);
	    }
	}
    }
    return vicinVals;
};

// expose(async function smooth(coord, mat) {
//     var vicinVals = getVicinVals(global.env.m, coord[1], coord[0], 4);
//     return [coord, Number(_.chain(vicinVals).countBy().toPairs().maxBy(_.last).head().value())];
// })
//
// self.onmessage = function(e) {
// 	let coord = e.data[0];
// 	let mat = e.data[1];
//     var vicinVals = getVicinVals(global.env.m, coord[1], coord[0], 4);
//     self.postMessage([coord, Number(_.chain(vicinVals).countBy().toPairs().maxBy(_.last).head().value())]);
// 	self.close();
// }

run = function(e) {
	let coord = e.data[0];
	let mat = e.data[1];
    var vicinVals = getVicinVals(mat, coord[1], coord[0], 4);
    return [coord, Number(_.chain(vicinVals).countBy().toPairs().maxBy(_.last).head().value())];
}

// var smooth = function(mat) {
//     var width = mat[0].length;
//     var height = mat.length;
//     var simp = [];
//     for (var i = 0; i < height; i++) {
// 	simp[i] = new Array(width);
//     }
//     var coords = []
//     for (var y = 0; y < height; y++) {
//       for (var x = 0; x < width; x++) {
//         coords.push([y, x])
//       }
//     }
//     var p = new Parallel(coords, {evalPath: "eval.js", env: {m: mat}})
//     let log = function () { console.log(arguments); };
//     p.require("lodash.js");
//     p.require(getVicinVals);
//     function smoothWorker(coord) {
//       var vicinVals = getVicinVals(global.env.m, coord[1], coord[0], 4);
//       return Number(_.chain(vicinVals).countBy().toPairs().maxBy(_.last).head().value());
//     };
//     p.map(smoothWorker).then(log)
//     console.log("Done");
//     return simp;
// };
