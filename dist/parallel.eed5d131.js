// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"scripts/parallel.js":[function(require,module,exports) {
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function () {
  var isCommonJS = typeof module !== 'undefined' && module.exports;
  var isNode = !(typeof window !== 'undefined' && this === window);

  var setImmediate = setImmediate || function (cb) {
    setTimeout(cb, 0);
  };

  var Worker = self.Worker;
  var URL = typeof self !== 'undefined' ? self.URL ? self.URL : self.webkitURL : null;

  var _supports = !!(isNode || self.Worker); // node always supports parallel


  function extend(from, to) {
    if (!to) to = {};

    for (var i in from) {
      if (to[i] === undefined) to[i] = from[i];
    }

    return to;
  }

  function Operation() {
    this._callbacks = [];
    this._errCallbacks = [];
    this._resolved = 0;
    this._result = null;
  }

  Operation.prototype.resolve = function (err, res) {
    if (!err) {
      this._resolved = 1;
      this._result = res;

      for (var i = 0; i < this._callbacks.length; ++i) {
        this._callbacks[i](res);
      }
    } else {
      this._resolved = 2;
      this._result = err;

      for (var iE = 0; iE < this._errCallbacks.length; ++iE) {
        this._errCallbacks[iE](err);
      }
    }

    this._callbacks = [];
    this._errCallbacks = [];
  };

  Operation.prototype.then = function (cb, errCb) {
    if (this._resolved === 1) {
      // result
      if (cb) {
        cb(this._result);
      }

      return;
    }

    if (this._resolved === 2) {
      // error
      if (errCb) {
        errCb(this._result);
      }

      return;
    }

    if (cb) {
      this._callbacks[this._callbacks.length] = cb;
    }

    if (errCb) {
      this._errCallbacks[this._errCallbacks.length] = errCb;
    }

    return this;
  };

  var defaults = {
    evalPath: null,
    maxWorkers: navigator.hardwareConcurrency || 4,
    synchronous: true,
    env: {},
    envNamespace: 'env'
  };

  function Parallel(data, options) {
    this.data = data;
    this.options = extend(defaults, options);
    this.operation = new Operation();
    this.operation.resolve(null, this.data);
    this.requiredScripts = [];
    this.requiredFunctions = [];
  } // static method


  Parallel.isSupported = function () {
    return _supports;
  };

  Parallel.prototype.getWorkerSource = function (cb, env) {
    var that = this;
    var preStr = '';
    var i = 0;

    if (!isNode && this.requiredScripts.length !== 0) {
      preStr += "importScripts(\"".concat(this.requiredScripts.join('","'), "\");\r\n");
    }

    for (i = 0; i < this.requiredFunctions.length; ++i) {
      if (this.requiredFunctions[i].name) {
        preStr += "var ".concat(this.requiredFunctions[i].name, " = ").concat(this.requiredFunctions[i].fn.toString(), ";");
      } else {
        preStr += this.requiredFunctions[i].fn.toString();
      }
    }

    env = JSON.stringify(env || {});
    var ns = this.options.envNamespace;

    if (isNode) {
      return "".concat(preStr, "process.on(\"message\", function(e) {global.").concat(ns, " = ").concat(env, ";process.send(JSON.stringify((").concat(cb.toString(), ")(JSON.parse(e).data)))})");
    }

    return "".concat(preStr, "self.onmessage = function(e) {var global = {}; global.").concat(ns, " = ").concat(env, ";self.postMessage((").concat(cb.toString(), ")(e.data))}");
  };

  Parallel.prototype.require = function () {
    var args = Array.prototype.slice.call(arguments, 0);
    var func;

    for (var i = 0; i < args.length; i++) {
      func = args[i];

      if (typeof func === 'string') {
        this.requiredScripts.push(func);
      } else if (typeof func === 'function') {
        this.requiredFunctions.push({
          fn: func
        });
      } else if (_typeof(func) === 'object') {
        this.requiredFunctions.push(func);
      }
    }

    return this;
  };

  Parallel.prototype._spawnWorker = function (cb, env) {
    var wrk;
    var src = this.getWorkerSource(cb, env);

    if (isNode) {
      wrk = new Worker(this.options.evalPath);
      wrk.postMessage(src);
    } else {
      if (Worker === undefined) {
        return undefined;
      }

      try {
        if (this.requiredScripts.length !== 0) {
          if (this.options.evalPath !== null) {
            wrk = new Worker(this.options.evalPath);
            wrk.postMessage(src);
          } else {
            throw new Error("Can't use required scripts without eval.js!");
          }
        } else if (!URL) {
          throw new Error("Can't create a blob URL in this browser!");
        } else {
          var blob = new Blob([src], {
            type: 'text/javascript'
          });
          var url = URL.createObjectURL(blob);
          wrk = new Worker(url);
        }
      } catch (e) {
        if (this.options.evalPath !== null) {
          // blob/url unsupported, cross-origin error
          wrk = new Worker(this.options.evalPath);
          wrk.postMessage(src);
        } else {
          throw e;
        }
      }
    }

    return wrk;
  };

  Parallel.prototype.spawn = function (cb, env) {
    var that = this;
    var newOp = new Operation();
    env = extend(this.options.env, env || {});
    this.operation.then(function () {
      var wrk = that._spawnWorker(cb, env);

      if (wrk !== undefined) {
        wrk.onmessage = function (msg) {
          wrk.terminate();
          that.data = msg.data;
          newOp.resolve(null, that.data);
        };

        wrk.onerror = function (e) {
          wrk.terminate();
          newOp.resolve(e, null);
        };

        wrk.postMessage(that.data);
      } else if (that.options.synchronous) {
        setImmediate(function () {
          try {
            that.data = cb(that.data);
            newOp.resolve(null, that.data);
          } catch (e) {
            newOp.resolve(e, null);
          }
        });
      } else {
        throw new Error('Workers do not exist and synchronous operation not allowed!');
      }
    });
    this.operation = newOp;
    return this;
  };

  Parallel.prototype._spawnMapWorker = function (i, cb, done, env, wrk) {
    var that = this;
    if (!wrk) wrk = that._spawnWorker(cb, env);

    if (wrk !== undefined) {
      wrk.onmessage = function (msg) {
        that.data[i] = msg.data;
        done(null, wrk);
      };

      wrk.onerror = function (e) {
        wrk.terminate();
        done(e);
      };

      wrk.postMessage(that.data[i]);
    } else if (that.options.synchronous) {
      setImmediate(function () {
        that.data[i] = cb(that.data[i]);
        done();
      });
    } else {
      throw new Error('Workers do not exist and synchronous operation not allowed!');
    }
  };

  Parallel.prototype.map = function (cb, env) {
    env = extend(this.options.env, env || {});

    if (!this.data.length) {
      return this.spawn(cb, env);
    }

    var that = this;
    var startedOps = 0;
    var doneOps = 0;

    function done(err, wrk) {
      if (err) {
        newOp.resolve(err, null);
      } else if (++doneOps === that.data.length) {
        newOp.resolve(null, that.data);
        if (wrk) wrk.terminate();
      } else if (startedOps < that.data.length) {
        that._spawnMapWorker(startedOps++, cb, done, env, wrk);
      } else if (wrk) wrk.terminate();
    }

    var newOp = new Operation();
    this.operation.then(function () {
      for (; startedOps - doneOps < that.options.maxWorkers && startedOps < that.data.length; ++startedOps) {
        that._spawnMapWorker(startedOps, cb, done, env);
      }
    }, function (err) {
      newOp.resolve(err, null);
    });
    this.operation = newOp;
    return this;
  };

  Parallel.prototype._spawnReduceWorker = function (data, cb, done, env, wrk) {
    var that = this;
    if (!wrk) wrk = that._spawnWorker(cb, env);

    if (wrk !== undefined) {
      wrk.onmessage = function (msg) {
        that.data[that.data.length] = msg.data;
        done(null, wrk);
      };

      wrk.onerror = function (e) {
        wrk.terminate();
        done(e, null);
      };

      wrk.postMessage(data);
    } else if (that.options.synchronous) {
      setImmediate(function () {
        that.data[that.data.length] = cb(data);
        done();
      });
    } else {
      throw new Error('Workers do not exist and synchronous operation not allowed!');
    }
  };

  Parallel.prototype.reduce = function (cb, env) {
    env = extend(this.options.env, env || {});

    if (!this.data.length) {
      throw new Error("Can't reduce non-array data");
    }

    var runningWorkers = 0;
    var that = this;

    function done(err, wrk) {
      --runningWorkers;

      if (err) {
        newOp.resolve(err, null);
      } else if (that.data.length === 1 && runningWorkers === 0) {
        that.data = that.data[0];
        newOp.resolve(null, that.data);
        if (wrk) wrk.terminate();
      } else if (that.data.length > 1) {
        ++runningWorkers;

        that._spawnReduceWorker([that.data[0], that.data[1]], cb, done, env, wrk);

        that.data.splice(0, 2);
      } else if (wrk) wrk.terminate();
    }

    var newOp = new Operation();
    this.operation.then(function () {
      if (that.data.length === 1) {
        newOp.resolve(null, that.data[0]);
      } else {
        for (var i = 0; i < that.options.maxWorkers && i < Math.floor(that.data.length / 2); ++i) {
          ++runningWorkers;

          that._spawnReduceWorker([that.data[i * 2], that.data[i * 2 + 1]], cb, done, env);
        }

        that.data.splice(0, i * 2);
      }
    });
    this.operation = newOp;
    return this;
  };

  Parallel.prototype.then = function (cb, errCb) {
    var that = this;
    var newOp = new Operation();
    errCb = typeof errCb === 'function' ? errCb : function () {};
    this.operation.then(function () {
      var retData;

      try {
        if (cb) {
          retData = cb(that.data);

          if (retData !== undefined) {
            that.data = retData;
          }
        }

        newOp.resolve(null, that.data);
      } catch (e) {
        if (errCb) {
          retData = errCb(e);

          if (retData !== undefined) {
            that.data = retData;
          }

          newOp.resolve(null, that.data);
        } else {
          newOp.resolve(null, e);
        }
      }
    }, function (err) {
      if (errCb) {
        var retData = errCb(err);

        if (retData !== undefined) {
          that.data = retData;
        }

        newOp.resolve(null, that.data);
      } else {
        newOp.resolve(null, err);
      }
    });
    this.operation = newOp;
    return this;
  };

  if (isCommonJS) {
    module.exports = Parallel;
  } else {
    self.Parallel = Parallel;
  }
})();
},{}],"../../../../../../../usr/local/lib/node_modules/parcel/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "61865" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else if (location.reload) {
        // `location` global exists in a web worker context but lacks `.reload()` function.
        location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["../../../../../../../usr/local/lib/node_modules/parcel/src/builtins/hmr-runtime.js","scripts/parallel.js"], null)
//# sourceMappingURL=/parallel.eed5d131.js.map