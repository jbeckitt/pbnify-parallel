// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"../../../../../../../usr/local/lib/node_modules/parcel/node_modules/process/browser.js":[function(require,module,exports) {

// shim for using process in browser
var process = module.exports = {}; // cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
  throw new Error('setTimeout has not been defined');
}

function defaultClearTimeout() {
  throw new Error('clearTimeout has not been defined');
}

(function () {
  try {
    if (typeof setTimeout === 'function') {
      cachedSetTimeout = setTimeout;
    } else {
      cachedSetTimeout = defaultSetTimout;
    }
  } catch (e) {
    cachedSetTimeout = defaultSetTimout;
  }

  try {
    if (typeof clearTimeout === 'function') {
      cachedClearTimeout = clearTimeout;
    } else {
      cachedClearTimeout = defaultClearTimeout;
    }
  } catch (e) {
    cachedClearTimeout = defaultClearTimeout;
  }
})();

function runTimeout(fun) {
  if (cachedSetTimeout === setTimeout) {
    //normal enviroments in sane situations
    return setTimeout(fun, 0);
  } // if setTimeout wasn't available but was latter defined


  if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
    cachedSetTimeout = setTimeout;
    return setTimeout(fun, 0);
  }

  try {
    // when when somebody has screwed with setTimeout but no I.E. maddness
    return cachedSetTimeout(fun, 0);
  } catch (e) {
    try {
      // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
      return cachedSetTimeout.call(null, fun, 0);
    } catch (e) {
      // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
      return cachedSetTimeout.call(this, fun, 0);
    }
  }
}

function runClearTimeout(marker) {
  if (cachedClearTimeout === clearTimeout) {
    //normal enviroments in sane situations
    return clearTimeout(marker);
  } // if clearTimeout wasn't available but was latter defined


  if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
    cachedClearTimeout = clearTimeout;
    return clearTimeout(marker);
  }

  try {
    // when when somebody has screwed with setTimeout but no I.E. maddness
    return cachedClearTimeout(marker);
  } catch (e) {
    try {
      // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
      return cachedClearTimeout.call(null, marker);
    } catch (e) {
      // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
      // Some versions of I.E. have different rules for clearTimeout vs setTimeout
      return cachedClearTimeout.call(this, marker);
    }
  }
}

var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
  if (!draining || !currentQueue) {
    return;
  }

  draining = false;

  if (currentQueue.length) {
    queue = currentQueue.concat(queue);
  } else {
    queueIndex = -1;
  }

  if (queue.length) {
    drainQueue();
  }
}

function drainQueue() {
  if (draining) {
    return;
  }

  var timeout = runTimeout(cleanUpNextTick);
  draining = true;
  var len = queue.length;

  while (len) {
    currentQueue = queue;
    queue = [];

    while (++queueIndex < len) {
      if (currentQueue) {
        currentQueue[queueIndex].run();
      }
    }

    queueIndex = -1;
    len = queue.length;
  }

  currentQueue = null;
  draining = false;
  runClearTimeout(timeout);
}

process.nextTick = function (fun) {
  var args = new Array(arguments.length - 1);

  if (arguments.length > 1) {
    for (var i = 1; i < arguments.length; i++) {
      args[i - 1] = arguments[i];
    }
  }

  queue.push(new Item(fun, args));

  if (queue.length === 1 && !draining) {
    runTimeout(drainQueue);
  }
}; // v8 likes predictible objects


function Item(fun, array) {
  this.fun = fun;
  this.array = array;
}

Item.prototype.run = function () {
  this.fun.apply(null, this.array);
};

process.title = 'browser';
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues

process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) {
  return [];
};

process.binding = function (name) {
  throw new Error('process.binding is not supported');
};

process.cwd = function () {
  return '/';
};

process.chdir = function (dir) {
  throw new Error('process.chdir is not supported');
};

process.umask = function () {
  return 0;
};
},{}],"scripts/threads-bundle.js":[function(require,module,exports) {
var global = arguments[3];
var process = require("process");
function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _wrapNativeSuper(Class) { var _cache = typeof Map === "function" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== "function") { throw new TypeError("Super expression must either be null or a function"); } if (typeof _cache !== "undefined") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }

function _construct(Parent, args, Class) { if (_isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _isNativeFunction(fn) { return Function.toString.call(fn).indexOf("[native code]") !== -1; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function () { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _createForOfIteratorHelper(o) { if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (o = _unsupportedIterableToArray(o))) { var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e2) { throw _e2; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var it, normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e3) { didErr = true; err = _e3; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function () {
  function r(e, n, t) {
    function o(i, f) {
      if (!n[i]) {
        if (!e[i]) {
          var c = "function" == typeof require && require;
          if (!f && c) return c(i, !0);
          if (u) return u(i, !0);
          var a = new Error("Cannot find module '" + i + "'");
          throw a.code = "MODULE_NOT_FOUND", a;
        }

        var p = n[i] = {
          exports: {}
        };
        e[i][0].call(p.exports, function (r) {
          var n = e[i][1][r];
          return o(n || r);
        }, p, p.exports, r, e, n, t);
      }

      return n[i].exports;
    }

    for (var u = "function" == typeof require && require, i = 0; i < t.length; i++) {
      o(t[i]);
    }

    return o;
  }

  return r;
})()({
  1: [function (require, module, exports) {
    (function (global) {
      "use strict";

      var _threads = require("threads");

      global.window.spawn = _threads.spawn;
      global.window.Pool = _threads.Pool;
      global.window.Worker = _threads.Worker;
    }).call(this, typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {});
  }, {
    "threads": 22
  }],
  2: [function (require, module, exports) {
    'use strict';

    var symbolObservable = require('symbol-observable').default;

    module.exports = function (value) {
      return Boolean(value && value[symbolObservable] && value === value[symbolObservable]());
    };
  }, {
    "symbol-observable": 19
  }],
  3: [function (require, module, exports) {
    /**
     * Helpers.
     */
    var s = 1000;
    var m = s * 60;
    var h = m * 60;
    var d = h * 24;
    var w = d * 7;
    var y = d * 365.25;
    /**
     * Parse or format the given `val`.
     *
     * Options:
     *
     *  - `long` verbose formatting [false]
     *
     * @param {String|Number} val
     * @param {Object} [options]
     * @throws {Error} throw an error if val is not a non-empty string or a number
     * @return {String|Number}
     * @api public
     */

    module.exports = function (val, options) {
      options = options || {};

      var type = _typeof(val);

      if (type === 'string' && val.length > 0) {
        return parse(val);
      } else if (type === 'number' && isFinite(val)) {
        return options.long ? fmtLong(val) : fmtShort(val);
      }

      throw new Error('val is not a non-empty string or a valid number. val=' + JSON.stringify(val));
    };
    /**
     * Parse the given `str` and return milliseconds.
     *
     * @param {String} str
     * @return {Number}
     * @api private
     */


    function parse(str) {
      str = String(str);

      if (str.length > 100) {
        return;
      }

      var match = /^(-?(?:\d+)?\.?\d+) *(milliseconds?|msecs?|ms|seconds?|secs?|s|minutes?|mins?|m|hours?|hrs?|h|days?|d|weeks?|w|years?|yrs?|y)?$/i.exec(str);

      if (!match) {
        return;
      }

      var n = parseFloat(match[1]);
      var type = (match[2] || 'ms').toLowerCase();

      switch (type) {
        case 'years':
        case 'year':
        case 'yrs':
        case 'yr':
        case 'y':
          return n * y;

        case 'weeks':
        case 'week':
        case 'w':
          return n * w;

        case 'days':
        case 'day':
        case 'd':
          return n * d;

        case 'hours':
        case 'hour':
        case 'hrs':
        case 'hr':
        case 'h':
          return n * h;

        case 'minutes':
        case 'minute':
        case 'mins':
        case 'min':
        case 'm':
          return n * m;

        case 'seconds':
        case 'second':
        case 'secs':
        case 'sec':
        case 's':
          return n * s;

        case 'milliseconds':
        case 'millisecond':
        case 'msecs':
        case 'msec':
        case 'ms':
          return n;

        default:
          return undefined;
      }
    }
    /**
     * Short format for `ms`.
     *
     * @param {Number} ms
     * @return {String}
     * @api private
     */


    function fmtShort(ms) {
      var msAbs = Math.abs(ms);

      if (msAbs >= d) {
        return Math.round(ms / d) + 'd';
      }

      if (msAbs >= h) {
        return Math.round(ms / h) + 'h';
      }

      if (msAbs >= m) {
        return Math.round(ms / m) + 'm';
      }

      if (msAbs >= s) {
        return Math.round(ms / s) + 's';
      }

      return ms + 'ms';
    }
    /**
     * Long format for `ms`.
     *
     * @param {Number} ms
     * @return {String}
     * @api private
     */


    function fmtLong(ms) {
      var msAbs = Math.abs(ms);

      if (msAbs >= d) {
        return plural(ms, msAbs, d, 'day');
      }

      if (msAbs >= h) {
        return plural(ms, msAbs, h, 'hour');
      }

      if (msAbs >= m) {
        return plural(ms, msAbs, m, 'minute');
      }

      if (msAbs >= s) {
        return plural(ms, msAbs, s, 'second');
      }

      return ms + ' ms';
    }
    /**
     * Pluralization helper.
     */


    function plural(ms, msAbs, n, name) {
      var isPlural = msAbs >= n * 1.5;
      return Math.round(ms / n) + ' ' + name + (isPlural ? 's' : '');
    }
  }, {}],
  4: [function (require, module, exports) {
    "use strict";

    var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
      return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }

        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }

        function step(result) {
          result.done ? resolve(result.value) : new P(function (resolve) {
            resolve(result.value);
          }).then(fulfilled, rejected);
        }

        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var AsyncSerialScheduler = /*#__PURE__*/function () {
      function AsyncSerialScheduler(observer) {
        _classCallCheck(this, AsyncSerialScheduler);

        this._baseObserver = observer;
        this._pendingPromises = new Set();
      }

      _createClass(AsyncSerialScheduler, [{
        key: "complete",
        value: function complete() {
          var _this = this;

          Promise.all(this._pendingPromises).then(function () {
            return _this._baseObserver.complete();
          }).catch(function (error) {
            return _this._baseObserver.error(error);
          });
        }
      }, {
        key: "error",
        value: function error(_error) {
          this._baseObserver.error(_error);
        }
      }, {
        key: "schedule",
        value: function schedule(task) {
          var _this2 = this;

          var prevPromisesCompletion = Promise.all(this._pendingPromises);
          var values = [];

          var next = function next(value) {
            return values.push(value);
          };

          var promise = Promise.resolve().then(function () {
            return __awaiter(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _iterator, _step, value;

              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return prevPromisesCompletion;

                    case 2:
                      _context.next = 4;
                      return task(next);

                    case 4:
                      this._pendingPromises.delete(promise);

                      _iterator = _createForOfIteratorHelper(values);

                      try {
                        for (_iterator.s(); !(_step = _iterator.n()).done;) {
                          value = _step.value;

                          this._baseObserver.next(value);
                        }
                      } catch (err) {
                        _iterator.e(err);
                      } finally {
                        _iterator.f();
                      }

                    case 7:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }).catch(function (error) {
            _this2._pendingPromises.delete(promise);

            _this2._baseObserver.error(error);
          });

          this._pendingPromises.add(promise);
        }
      }]);

      return AsyncSerialScheduler;
    }();

    exports.AsyncSerialScheduler = AsyncSerialScheduler;
  }, {}],
  5: [function (require, module, exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    exports.hasSymbols = function () {
      return typeof Symbol === "function";
    };

    exports.hasSymbol = function (name) {
      return exports.hasSymbols() && Boolean(Symbol[name]);
    };

    exports.getSymbol = function (name) {
      return exports.hasSymbol(name) ? Symbol[name] : "@@" + name;
    };

    function registerObservableSymbol() {
      if (exports.hasSymbols() && !exports.hasSymbol("observable")) {
        Symbol.observable = Symbol("observable");
      }
    }

    exports.registerObservableSymbol = registerObservableSymbol;

    if (!exports.hasSymbol("asyncIterator")) {
      Symbol.asyncIterator = Symbol.asyncIterator || Symbol.for("Symbol.asyncIterator");
    }
  }, {}],
  6: [function (require, module, exports) {
    "use strict"; /// <reference lib="es2018" />

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var _symbols_1 = require("./_symbols");

    function isAsyncIterator(thing) {
      return thing && _symbols_1.hasSymbol("asyncIterator") && thing[Symbol.asyncIterator];
    }

    exports.isAsyncIterator = isAsyncIterator;

    function isIterator(thing) {
      return thing && _symbols_1.hasSymbol("iterator") && thing[Symbol.iterator];
    }

    exports.isIterator = isIterator;
  }, {
    "./_symbols": 5
  }],
  7: [function (require, module, exports) {
    "use strict";

    var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
      return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }

        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }

        function step(result) {
          result.done ? resolve(result.value) : new P(function (resolve) {
            resolve(result.value);
          }).then(fulfilled, rejected);
        }

        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var _scheduler_1 = require("./_scheduler");

    var observable_1 = require("./observable");

    var unsubscribe_1 = require("./unsubscribe");
    /**
     * Filters the values emitted by another observable.
     * To be applied to an input observable using `pipe()`.
     */


    function filter(test) {
      return function (observable) {
        return new observable_1.default(function (observer) {
          var scheduler = new _scheduler_1.AsyncSerialScheduler(observer);
          var subscription = observable.subscribe({
            complete: function complete() {
              scheduler.complete();
            },
            error: function error(_error2) {
              scheduler.error(_error2);
            },
            next: function next(input) {
              var _this3 = this;

              scheduler.schedule(function (next) {
                return __awaiter(_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                  return regeneratorRuntime.wrap(function _callee2$(_context2) {
                    while (1) {
                      switch (_context2.prev = _context2.next) {
                        case 0:
                          _context2.next = 2;
                          return test(input);

                        case 2:
                          if (!_context2.sent) {
                            _context2.next = 4;
                            break;
                          }

                          next(input);

                        case 4:
                        case "end":
                          return _context2.stop();
                      }
                    }
                  }, _callee2);
                }));
              });
            }
          });
          return function () {
            return unsubscribe_1.default(subscription);
          };
        });
      };
    }

    exports.default = filter;
  }, {
    "./_scheduler": 4,
    "./observable": 14,
    "./unsubscribe": 17
  }],
  8: [function (require, module, exports) {
    "use strict";

    var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
      return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }

        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }

        function step(result) {
          result.done ? resolve(result.value) : new P(function (resolve) {
            resolve(result.value);
          }).then(fulfilled, rejected);
        }

        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };

    var __asyncValues = this && this.__asyncValues || function (o) {
      if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
      var m = o[Symbol.asyncIterator],
          i;
      return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
        return this;
      }, i);

      function verb(n) {
        i[n] = o[n] && function (v) {
          return new Promise(function (resolve, reject) {
            v = o[n](v), settle(resolve, reject, v.done, v.value);
          });
        };
      }

      function settle(resolve, reject, d, v) {
        Promise.resolve(v).then(function (v) {
          resolve({
            value: v,
            done: d
          });
        }, reject);
      }
    };

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var _scheduler_1 = require("./_scheduler");

    var _util_1 = require("./_util");

    var observable_1 = require("./observable");

    var unsubscribe_1 = require("./unsubscribe");
    /**
     * Maps the values emitted by another observable. In contrast to `map()`
     * the `mapper` function returns an array of values that will be emitted
     * separately.
     * Use `flatMap()` to map input values to zero, one or multiple output
     * values. To be applied to an input observable using `pipe()`.
     */


    function flatMap(mapper) {
      return function (observable) {
        return new observable_1.default(function (observer) {
          var scheduler = new _scheduler_1.AsyncSerialScheduler(observer);
          var subscription = observable.subscribe({
            complete: function complete() {
              scheduler.complete();
            },
            error: function error(_error3) {
              scheduler.error(_error3);
            },
            next: function next(input) {
              var _this4 = this;

              scheduler.schedule(function (next) {
                return __awaiter(_this4, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                  var e_1, _a, mapped, mapped_1, mapped_1_1, element;

                  return regeneratorRuntime.wrap(function _callee3$(_context3) {
                    while (1) {
                      switch (_context3.prev = _context3.next) {
                        case 0:
                          _context3.next = 2;
                          return mapper(input);

                        case 2:
                          mapped = _context3.sent;

                          if (!(_util_1.isIterator(mapped) || _util_1.isAsyncIterator(mapped))) {
                            _context3.next = 31;
                            break;
                          }

                          _context3.prev = 4;
                          mapped_1 = __asyncValues(mapped);

                        case 6:
                          _context3.next = 8;
                          return mapped_1.next();

                        case 8:
                          mapped_1_1 = _context3.sent;

                          if (mapped_1_1.done) {
                            _context3.next = 14;
                            break;
                          }

                          element = mapped_1_1.value;
                          next(element);

                        case 12:
                          _context3.next = 6;
                          break;

                        case 14:
                          _context3.next = 19;
                          break;

                        case 16:
                          _context3.prev = 16;
                          _context3.t0 = _context3["catch"](4);
                          e_1 = {
                            error: _context3.t0
                          };

                        case 19:
                          _context3.prev = 19;
                          _context3.prev = 20;

                          if (!(mapped_1_1 && !mapped_1_1.done && (_a = mapped_1.return))) {
                            _context3.next = 24;
                            break;
                          }

                          _context3.next = 24;
                          return _a.call(mapped_1);

                        case 24:
                          _context3.prev = 24;

                          if (!e_1) {
                            _context3.next = 27;
                            break;
                          }

                          throw e_1.error;

                        case 27:
                          return _context3.finish(24);

                        case 28:
                          return _context3.finish(19);

                        case 29:
                          _context3.next = 32;
                          break;

                        case 31:
                          mapped.map(function (output) {
                            return next(output);
                          });

                        case 32:
                        case "end":
                          return _context3.stop();
                      }
                    }
                  }, _callee3, null, [[4, 16, 19, 29], [20,, 24, 28]]);
                }));
              });
            }
          });
          return function () {
            return unsubscribe_1.default(subscription);
          };
        });
      };
    }

    exports.default = flatMap;
  }, {
    "./_scheduler": 4,
    "./_util": 6,
    "./observable": 14,
    "./unsubscribe": 17
  }],
  9: [function (require, module, exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var filter_1 = require("./filter");

    exports.filter = filter_1.default;

    var flatMap_1 = require("./flatMap");

    exports.flatMap = flatMap_1.default;

    var interval_1 = require("./interval");

    exports.interval = interval_1.default;

    var map_1 = require("./map");

    exports.map = map_1.default;

    var merge_1 = require("./merge");

    exports.merge = merge_1.default;

    var multicast_1 = require("./multicast");

    exports.multicast = multicast_1.default;

    var observable_1 = require("./observable");

    exports.Observable = observable_1.default;

    var scan_1 = require("./scan");

    exports.scan = scan_1.default;

    var subject_1 = require("./subject");

    exports.Subject = subject_1.default;

    var unsubscribe_1 = require("./unsubscribe");

    exports.unsubscribe = unsubscribe_1.default;
  }, {
    "./filter": 7,
    "./flatMap": 8,
    "./interval": 10,
    "./map": 11,
    "./merge": 12,
    "./multicast": 13,
    "./observable": 14,
    "./scan": 15,
    "./subject": 16,
    "./unsubscribe": 17
  }],
  10: [function (require, module, exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var observable_1 = require("./observable");
    /**
     * Creates an observable that yields a new value every `period` milliseconds.
     * The first value emitted is 0, then 1, 2, etc. The first value is not emitted
     * immediately, but after the first interval.
     */


    function interval(period) {
      return new observable_1.Observable(function (observer) {
        var counter = 0;
        var handle = setInterval(function () {
          observer.next(counter++);
        }, period);
        return function () {
          return clearInterval(handle);
        };
      });
    }

    exports.default = interval;
  }, {
    "./observable": 14
  }],
  11: [function (require, module, exports) {
    "use strict";

    var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
      return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }

        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }

        function step(result) {
          result.done ? resolve(result.value) : new P(function (resolve) {
            resolve(result.value);
          }).then(fulfilled, rejected);
        }

        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var _scheduler_1 = require("./_scheduler");

    var observable_1 = require("./observable");

    var unsubscribe_1 = require("./unsubscribe");
    /**
     * Maps the values emitted by another observable to different values.
     * To be applied to an input observable using `pipe()`.
     */


    function map(mapper) {
      return function (observable) {
        return new observable_1.default(function (observer) {
          var scheduler = new _scheduler_1.AsyncSerialScheduler(observer);
          var subscription = observable.subscribe({
            complete: function complete() {
              scheduler.complete();
            },
            error: function error(_error4) {
              scheduler.error(_error4);
            },
            next: function next(input) {
              var _this5 = this;

              scheduler.schedule(function (next) {
                return __awaiter(_this5, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
                  var mapped;
                  return regeneratorRuntime.wrap(function _callee4$(_context4) {
                    while (1) {
                      switch (_context4.prev = _context4.next) {
                        case 0:
                          _context4.next = 2;
                          return mapper(input);

                        case 2:
                          mapped = _context4.sent;
                          next(mapped);

                        case 4:
                        case "end":
                          return _context4.stop();
                      }
                    }
                  }, _callee4);
                }));
              });
            }
          });
          return function () {
            return unsubscribe_1.default(subscription);
          };
        });
      };
    }

    exports.default = map;
  }, {
    "./_scheduler": 4,
    "./observable": 14,
    "./unsubscribe": 17
  }],
  12: [function (require, module, exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var observable_1 = require("./observable");

    var unsubscribe_1 = require("./unsubscribe");

    function merge() {
      for (var _len = arguments.length, observables = new Array(_len), _key = 0; _key < _len; _key++) {
        observables[_key] = arguments[_key];
      }

      if (observables.length === 0) {
        return observable_1.Observable.from([]);
      }

      return new observable_1.Observable(function (observer) {
        var completed = 0;
        var subscriptions = observables.map(function (input) {
          return input.subscribe({
            error: function error(_error5) {
              observer.error(_error5);
              unsubscribeAll();
            },
            next: function next(value) {
              observer.next(value);
            },
            complete: function complete() {
              if (++completed === observables.length) {
                observer.complete();
                unsubscribeAll();
              }
            }
          });
        });

        var unsubscribeAll = function unsubscribeAll() {
          subscriptions.forEach(function (subscription) {
            return unsubscribe_1.default(subscription);
          });
        };

        return unsubscribeAll;
      });
    }

    exports.default = merge;
  }, {
    "./observable": 14,
    "./unsubscribe": 17
  }],
  13: [function (require, module, exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var observable_1 = require("./observable");

    var subject_1 = require("./subject");

    var unsubscribe_1 = require("./unsubscribe"); // TODO: Subject already creates additional observables "under the hood",
    //       now we introduce even more. A true native MulticastObservable
    //       would be preferable.

    /**
     * Takes a "cold" observable and returns a wrapping "hot" observable that
     * proxies the input observable's values and errors.
     *
     * An observable is called "cold" when its initialization function is run
     * for each new subscriber. This is how observable-fns's `Observable`
     * implementation works.
     *
     * A hot observable is an observable where new subscribers subscribe to
     * the upcoming values of an already-initialiazed observable.
     *
     * The multicast observable will lazily subscribe to the source observable
     * once it has its first own subscriber and will unsubscribe from the
     * source observable when its last own subscriber unsubscribed.
     */


    function multicast(coldObservable) {
      var subject = new subject_1.default();
      var sourceSubscription;
      var subscriberCount = 0;
      return new observable_1.default(function (observer) {
        // Init source subscription lazily
        if (!sourceSubscription) {
          sourceSubscription = coldObservable.subscribe(subject);
        } // Pipe all events from `subject` into this observable


        var subscription = subject.subscribe(observer);
        subscriberCount++;
        return function () {
          subscriberCount--;
          subscription.unsubscribe(); // Close source subscription once last subscriber has unsubscribed

          if (subscriberCount === 0) {
            unsubscribe_1.default(sourceSubscription);
            sourceSubscription = undefined;
          }
        };
      });
    }

    exports.default = multicast;
  }, {
    "./observable": 14,
    "./subject": 16,
    "./unsubscribe": 17
  }],
  14: [function (require, module, exports) {
    "use strict";
    /**
     * Based on <https://raw.githubusercontent.com/zenparsing/zen-observable/master/src/Observable.js>
     * At commit: f63849a8c60af5d514efc8e9d6138d8273c49ad6
     */

    Object.defineProperty(exports, "__esModule", {
      value: true
    }); /// <reference path="../types/symbols.d.ts" />

    var _symbols_1 = require("./_symbols");

    var SymbolIterator = _symbols_1.getSymbol("iterator");

    var SymbolObservable = _symbols_1.getSymbol("observable");

    var SymbolSpecies = _symbols_1.getSymbol("species"); // === Abstract Operations ===


    function getMethod(obj, key) {
      var value = obj[key];

      if (value == null) {
        return undefined;
      }

      if (typeof value !== "function") {
        throw new TypeError(value + " is not a function");
      }

      return value;
    }

    function getSpecies(obj) {
      var ctor = obj.constructor;

      if (ctor !== undefined) {
        ctor = ctor[SymbolSpecies];

        if (ctor === null) {
          ctor = undefined;
        }
      }

      return ctor !== undefined ? ctor : Observable;
    }

    function isObservable(x) {
      return x instanceof Observable; // SPEC: Brand check
    }

    function hostReportError(error) {
      if (hostReportError.log) {
        hostReportError.log(error);
      } else {
        setTimeout(function () {
          throw error;
        }, 0);
      }
    }

    function enqueue(fn) {
      Promise.resolve().then(function () {
        try {
          fn();
        } catch (e) {
          hostReportError(e);
        }
      });
    }

    function cleanupSubscription(subscription) {
      var cleanup = subscription._cleanup;

      if (cleanup === undefined) {
        return;
      }

      subscription._cleanup = undefined;

      if (!cleanup) {
        return;
      }

      try {
        if (typeof cleanup === "function") {
          cleanup();
        } else {
          var unsubscribe = getMethod(cleanup, "unsubscribe");

          if (unsubscribe) {
            unsubscribe.call(cleanup);
          }
        }
      } catch (e) {
        hostReportError(e);
      }
    }

    function closeSubscription(subscription) {
      subscription._observer = undefined;
      subscription._queue = undefined;
      subscription._state = "closed";
    }

    function flushSubscription(subscription) {
      var queue = subscription._queue;

      if (!queue) {
        return;
      }

      subscription._queue = undefined;
      subscription._state = "ready";

      var _iterator2 = _createForOfIteratorHelper(queue),
          _step2;

      try {
        for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
          var item = _step2.value;
          notifySubscription(subscription, item.type, item.value);

          if (subscription._state === "closed") {
            break;
          }
        }
      } catch (err) {
        _iterator2.e(err);
      } finally {
        _iterator2.f();
      }
    }

    function notifySubscription(subscription, type, value) {
      subscription._state = "running";
      var observer = subscription._observer;

      try {
        var m = observer ? getMethod(observer, type) : undefined;

        switch (type) {
          case "next":
            if (m) m.call(observer, value);
            break;

          case "error":
            closeSubscription(subscription);
            if (m) m.call(observer, value);else throw value;
            break;

          case "complete":
            closeSubscription(subscription);
            if (m) m.call(observer);
            break;
        }
      } catch (e) {
        hostReportError(e);
      }

      if (subscription._state === "closed") {
        cleanupSubscription(subscription);
      } else if (subscription._state === "running") {
        subscription._state = "ready";
      }
    }

    function onNotify(subscription, type, value) {
      if (subscription._state === "closed") {
        return;
      }

      if (subscription._state === "buffering") {
        subscription._queue = subscription._queue || [];

        subscription._queue.push({
          type: type,
          value: value
        });

        return;
      }

      if (subscription._state !== "ready") {
        subscription._state = "buffering";
        subscription._queue = [{
          type: type,
          value: value
        }];
        enqueue(function () {
          return flushSubscription(subscription);
        });
        return;
      }

      notifySubscription(subscription, type, value);
    }

    var Subscription = /*#__PURE__*/function () {
      function Subscription(observer, subscriber) {
        _classCallCheck(this, Subscription);

        // ASSERT: observer is an object
        // ASSERT: subscriber is callable
        this._cleanup = undefined;
        this._observer = observer;
        this._queue = undefined;
        this._state = "initializing";
        var subscriptionObserver = new SubscriptionObserver(this);

        try {
          this._cleanup = subscriber.call(undefined, subscriptionObserver);
        } catch (e) {
          subscriptionObserver.error(e);
        }

        if (this._state === "initializing") {
          this._state = "ready";
        }
      }

      _createClass(Subscription, [{
        key: "unsubscribe",
        value: function unsubscribe() {
          if (this._state !== "closed") {
            closeSubscription(this);
            cleanupSubscription(this);
          }
        }
      }, {
        key: "closed",
        get: function get() {
          return this._state === "closed";
        }
      }]);

      return Subscription;
    }();

    exports.Subscription = Subscription;

    var SubscriptionObserver = /*#__PURE__*/function () {
      function SubscriptionObserver(subscription) {
        _classCallCheck(this, SubscriptionObserver);

        this._subscription = subscription;
      }

      _createClass(SubscriptionObserver, [{
        key: "next",
        value: function next(value) {
          onNotify(this._subscription, "next", value);
        }
      }, {
        key: "error",
        value: function error(value) {
          onNotify(this._subscription, "error", value);
        }
      }, {
        key: "complete",
        value: function complete() {
          onNotify(this._subscription, "complete");
        }
      }, {
        key: "closed",
        get: function get() {
          return this._subscription._state === "closed";
        }
      }]);

      return SubscriptionObserver;
    }();

    exports.SubscriptionObserver = SubscriptionObserver;
    /**
     * The basic Observable class. This primitive is used to wrap asynchronous
     * data streams in a common standardized data type that is interoperable
     * between libraries and can be composed to represent more complex processes.
     */

    var Observable = /*#__PURE__*/function () {
      function Observable(subscriber) {
        _classCallCheck(this, Observable);

        if (!(this instanceof Observable)) {
          throw new TypeError("Observable cannot be called as a function");
        }

        if (typeof subscriber !== "function") {
          throw new TypeError("Observable initializer must be a function");
        }

        this._subscriber = subscriber;
      }

      _createClass(Observable, [{
        key: "subscribe",
        value: function subscribe(nextOrObserver, onError, onComplete) {
          if (_typeof(nextOrObserver) !== "object" || nextOrObserver === null) {
            nextOrObserver = {
              next: nextOrObserver,
              error: onError,
              complete: onComplete
            };
          }

          return new Subscription(nextOrObserver, this._subscriber);
        }
      }, {
        key: "pipe",
        value: function pipe(first) {
          // tslint:disable-next-line no-this-assignment
          var intermediate = this;

          for (var _len2 = arguments.length, mappers = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
            mappers[_key2 - 1] = arguments[_key2];
          }

          for (var _i = 0, _arr = [first].concat(mappers); _i < _arr.length; _i++) {
            var mapper = _arr[_i];
            intermediate = mapper(intermediate);
          }

          return intermediate;
        }
      }, {
        key: "tap",
        value: function tap(nextOrObserver, onError, onComplete) {
          var _this6 = this;

          var tapObserver = _typeof(nextOrObserver) !== "object" || nextOrObserver === null ? {
            next: nextOrObserver,
            error: onError,
            complete: onComplete
          } : nextOrObserver;
          return new Observable(function (observer) {
            return _this6.subscribe({
              next: function next(value) {
                tapObserver.next && tapObserver.next(value);
                observer.next(value);
              },
              error: function error(_error6) {
                tapObserver.error && tapObserver.error(_error6);
                observer.error(_error6);
              },
              complete: function complete() {
                tapObserver.complete && tapObserver.complete();
                observer.complete();
              },
              start: function start(subscription) {
                tapObserver.start && tapObserver.start(subscription);
              }
            });
          });
        }
      }, {
        key: "forEach",
        value: function forEach(fn) {
          var _this7 = this;

          return new Promise(function (resolve, reject) {
            if (typeof fn !== "function") {
              reject(new TypeError(fn + " is not a function"));
              return;
            }

            function done() {
              subscription.unsubscribe();
              resolve();
            }

            var subscription = _this7.subscribe({
              next: function next(value) {
                try {
                  fn(value, done);
                } catch (e) {
                  reject(e);
                  subscription.unsubscribe();
                }
              },
              error: reject,
              complete: resolve
            });
          });
        }
      }, {
        key: "map",
        value: function map(fn) {
          var _this8 = this;

          if (typeof fn !== "function") {
            throw new TypeError(fn + " is not a function");
          }

          var C = getSpecies(this);
          return new C(function (observer) {
            return _this8.subscribe({
              next: function next(value) {
                var propagatedValue = value;

                try {
                  propagatedValue = fn(value);
                } catch (e) {
                  return observer.error(e);
                }

                observer.next(propagatedValue);
              },
              error: function error(e) {
                observer.error(e);
              },
              complete: function complete() {
                observer.complete();
              }
            });
          });
        }
      }, {
        key: "filter",
        value: function filter(fn) {
          var _this9 = this;

          if (typeof fn !== "function") {
            throw new TypeError(fn + " is not a function");
          }

          var C = getSpecies(this);
          return new C(function (observer) {
            return _this9.subscribe({
              next: function next(value) {
                try {
                  if (!fn(value)) return;
                } catch (e) {
                  return observer.error(e);
                }

                observer.next(value);
              },
              error: function error(e) {
                observer.error(e);
              },
              complete: function complete() {
                observer.complete();
              }
            });
          });
        }
      }, {
        key: "reduce",
        value: function reduce(fn, seed) {
          var _this10 = this;

          if (typeof fn !== "function") {
            throw new TypeError(fn + " is not a function");
          }

          var C = getSpecies(this);
          var hasSeed = arguments.length > 1;
          var hasValue = false;
          var acc = seed;
          return new C(function (observer) {
            return _this10.subscribe({
              next: function next(value) {
                var first = !hasValue;
                hasValue = true;

                if (!first || hasSeed) {
                  try {
                    acc = fn(acc, value);
                  } catch (e) {
                    return observer.error(e);
                  }
                } else {
                  acc = value;
                }
              },
              error: function error(e) {
                observer.error(e);
              },
              complete: function complete() {
                if (!hasValue && !hasSeed) {
                  return observer.error(new TypeError("Cannot reduce an empty sequence"));
                }

                observer.next(acc);
                observer.complete();
              }
            });
          });
        }
      }, {
        key: "concat",
        value: function concat() {
          var _this11 = this;

          for (var _len3 = arguments.length, sources = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
            sources[_key3] = arguments[_key3];
          }

          var C = getSpecies(this);
          return new C(function (observer) {
            var subscription;
            var index = 0;

            function startNext(next) {
              subscription = next.subscribe({
                next: function next(v) {
                  observer.next(v);
                },
                error: function error(e) {
                  observer.error(e);
                },
                complete: function complete() {
                  if (index === sources.length) {
                    subscription = undefined;
                    observer.complete();
                  } else {
                    startNext(C.from(sources[index++]));
                  }
                }
              });
            }

            startNext(_this11);
            return function () {
              if (subscription) {
                subscription.unsubscribe();
                subscription = undefined;
              }
            };
          });
        }
      }, {
        key: "flatMap",
        value: function flatMap(fn) {
          var _this12 = this;

          if (typeof fn !== "function") {
            throw new TypeError(fn + " is not a function");
          }

          var C = getSpecies(this);
          return new C(function (observer) {
            var subscriptions = [];

            var outer = _this12.subscribe({
              next: function next(value) {
                var normalizedValue;

                if (fn) {
                  try {
                    normalizedValue = fn(value);
                  } catch (e) {
                    return observer.error(e);
                  }
                } else {
                  normalizedValue = value;
                }

                var inner = C.from(normalizedValue).subscribe({
                  next: function next(innerValue) {
                    observer.next(innerValue);
                  },
                  error: function error(e) {
                    observer.error(e);
                  },
                  complete: function complete() {
                    var i = subscriptions.indexOf(inner);
                    if (i >= 0) subscriptions.splice(i, 1);
                    completeIfDone();
                  }
                });
                subscriptions.push(inner);
              },
              error: function error(e) {
                observer.error(e);
              },
              complete: function complete() {
                completeIfDone();
              }
            });

            function completeIfDone() {
              if (outer.closed && subscriptions.length === 0) {
                observer.complete();
              }
            }

            return function () {
              subscriptions.forEach(function (s) {
                return s.unsubscribe();
              });
              outer.unsubscribe();
            };
          });
        }
      }, {
        key: SymbolObservable,
        value: function value() {
          return this;
        }
      }], [{
        key: "from",
        value: function from(x) {
          var C = typeof this === "function" ? this : Observable;

          if (x == null) {
            throw new TypeError(x + " is not an object");
          }

          var observableMethod = getMethod(x, SymbolObservable);

          if (observableMethod) {
            var observable = observableMethod.call(x);

            if (Object(observable) !== observable) {
              throw new TypeError(observable + " is not an object");
            }

            if (isObservable(observable) && observable.constructor === C) {
              return observable;
            }

            return new C(function (observer) {
              return observable.subscribe(observer);
            });
          }

          if (_symbols_1.hasSymbol("iterator")) {
            var iteratorMethod = getMethod(x, SymbolIterator);

            if (iteratorMethod) {
              return new C(function (observer) {
                enqueue(function () {
                  if (observer.closed) return;

                  var _iterator3 = _createForOfIteratorHelper(iteratorMethod.call(x)),
                      _step3;

                  try {
                    for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
                      var item = _step3.value;
                      observer.next(item);
                      if (observer.closed) return;
                    }
                  } catch (err) {
                    _iterator3.e(err);
                  } finally {
                    _iterator3.f();
                  }

                  observer.complete();
                });
              });
            }
          }

          if (Array.isArray(x)) {
            return new C(function (observer) {
              enqueue(function () {
                if (observer.closed) return;

                var _iterator4 = _createForOfIteratorHelper(x),
                    _step4;

                try {
                  for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
                    var item = _step4.value;
                    observer.next(item);
                    if (observer.closed) return;
                  }
                } catch (err) {
                  _iterator4.e(err);
                } finally {
                  _iterator4.f();
                }

                observer.complete();
              });
            });
          }

          throw new TypeError(x + " is not observable");
        }
      }, {
        key: "of",
        value: function of() {
          for (var _len4 = arguments.length, items = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
            items[_key4] = arguments[_key4];
          }

          var C = typeof this === "function" ? this : Observable;
          return new C(function (observer) {
            enqueue(function () {
              if (observer.closed) return;

              var _iterator5 = _createForOfIteratorHelper(items),
                  _step5;

              try {
                for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
                  var item = _step5.value;
                  observer.next(item);
                  if (observer.closed) return;
                }
              } catch (err) {
                _iterator5.e(err);
              } finally {
                _iterator5.f();
              }

              observer.complete();
            });
          });
        }
      }, {
        key: SymbolSpecies,
        get: function get() {
          return this;
        }
      }]);

      return Observable;
    }();

    exports.Observable = Observable;

    if (_symbols_1.hasSymbols()) {
      Object.defineProperty(Observable, Symbol("extensions"), {
        value: {
          symbol: SymbolObservable,
          hostReportError: hostReportError
        },
        configurable: true
      });
    }

    exports.default = Observable;
  }, {
    "./_symbols": 5
  }],
  15: [function (require, module, exports) {
    "use strict";

    var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
      return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }

        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }

        function step(result) {
          result.done ? resolve(result.value) : new P(function (resolve) {
            resolve(result.value);
          }).then(fulfilled, rejected);
        }

        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var _scheduler_1 = require("./_scheduler");

    var observable_1 = require("./observable");

    var unsubscribe_1 = require("./unsubscribe");

    function scan(accumulator, seed) {
      return function (observable) {
        return new observable_1.default(function (observer) {
          var accumulated;
          var index = 0;
          var scheduler = new _scheduler_1.AsyncSerialScheduler(observer);
          var subscription = observable.subscribe({
            complete: function complete() {
              scheduler.complete();
            },
            error: function error(_error7) {
              scheduler.error(_error7);
            },
            next: function next(value) {
              var _this13 = this;

              scheduler.schedule(function (next) {
                return __awaiter(_this13, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
                  var prevAcc;
                  return regeneratorRuntime.wrap(function _callee5$(_context5) {
                    while (1) {
                      switch (_context5.prev = _context5.next) {
                        case 0:
                          prevAcc = index === 0 ? typeof seed === "undefined" ? value : seed : accumulated;
                          _context5.next = 3;
                          return accumulator(prevAcc, value, index++);

                        case 3:
                          accumulated = _context5.sent;
                          next(accumulated);

                        case 5:
                        case "end":
                          return _context5.stop();
                      }
                    }
                  }, _callee5);
                }));
              });
            }
          });
          return function () {
            return unsubscribe_1.default(subscription);
          };
        });
      };
    }

    exports.default = scan;
  }, {
    "./_scheduler": 4,
    "./observable": 14,
    "./unsubscribe": 17
  }],
  16: [function (require, module, exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var observable_1 = require("./observable"); // TODO: This observer iteration approach looks inelegant and expensive
    // Idea: Come up with super class for Subscription that contains the
    //       notify*, ... methods and use it here

    /**
     * A subject is a "hot" observable (see `multicast`) that has its observer
     * methods (`.next(value)`, `.error(error)`, `.complete()`) exposed.
     *
     * Be careful, though! With great power comes great responsibility. Only use
     * the `Subject` when you really need to trigger updates "from the outside" and
     * try to keep the code that can access it to a minimum. Return
     * `Observable.from(mySubject)` to not allow other code to mutate.
     */


    var MulticastSubject = /*#__PURE__*/function (_observable_1$default) {
      _inherits(MulticastSubject, _observable_1$default);

      var _super = _createSuper(MulticastSubject);

      function MulticastSubject() {
        var _this14;

        _classCallCheck(this, MulticastSubject);

        _this14 = _super.call(this, function (observer) {
          _this14._observers.add(observer);

          return function () {
            return _this14._observers.delete(observer);
          };
        });
        _this14._observers = new Set();
        return _this14;
      }

      _createClass(MulticastSubject, [{
        key: "next",
        value: function next(value) {
          var _iterator6 = _createForOfIteratorHelper(this._observers),
              _step6;

          try {
            for (_iterator6.s(); !(_step6 = _iterator6.n()).done;) {
              var observer = _step6.value;
              observer.next(value);
            }
          } catch (err) {
            _iterator6.e(err);
          } finally {
            _iterator6.f();
          }
        }
      }, {
        key: "error",
        value: function error(_error8) {
          var _iterator7 = _createForOfIteratorHelper(this._observers),
              _step7;

          try {
            for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
              var observer = _step7.value;
              observer.error(_error8);
            }
          } catch (err) {
            _iterator7.e(err);
          } finally {
            _iterator7.f();
          }
        }
      }, {
        key: "complete",
        value: function complete() {
          var _iterator8 = _createForOfIteratorHelper(this._observers),
              _step8;

          try {
            for (_iterator8.s(); !(_step8 = _iterator8.n()).done;) {
              var observer = _step8.value;
              observer.complete();
            }
          } catch (err) {
            _iterator8.e(err);
          } finally {
            _iterator8.f();
          }
        }
      }]);

      return MulticastSubject;
    }(observable_1.default);

    exports.default = MulticastSubject;
  }, {
    "./observable": 14
  }],
  17: [function (require, module, exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    /**
     * Unsubscribe from a subscription returned by something that looks like an observable,
     * but is not necessarily our observable implementation.
     */

    function unsubscribe(subscription) {
      if (typeof subscription === "function") {
        subscription();
      } else if (subscription && typeof subscription.unsubscribe === "function") {
        subscription.unsubscribe();
      }
    }

    exports.default = unsubscribe;
  }, {}],
  18: [function (require, module, exports) {
    module.exports = require("./dist/index.js");
  }, {
    "./dist/index.js": 9
  }],
  19: [function (require, module, exports) {
    (function (global) {
      'use strict';

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var _ponyfill = require('./ponyfill.js');

      var _ponyfill2 = _interopRequireDefault(_ponyfill);

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
          'default': obj
        };
      }

      var root;
      /* global window */

      if (typeof self !== 'undefined') {
        root = self;
      } else if (typeof window !== 'undefined') {
        root = window;
      } else if (typeof global !== 'undefined') {
        root = global;
      } else if (typeof module !== 'undefined') {
        root = module;
      } else {
        root = Function('return this')();
      }

      var result = (0, _ponyfill2['default'])(root);
      exports['default'] = result;
    }).call(this, typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {});
  }, {
    "./ponyfill.js": 20
  }],
  20: [function (require, module, exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports['default'] = symbolObservablePonyfill;

    function symbolObservablePonyfill(root) {
      var result;
      var _Symbol = root.Symbol;

      if (typeof _Symbol === 'function') {
        if (_Symbol.observable) {
          result = _Symbol.observable;
        } else {
          result = _Symbol('observable');
          _Symbol.observable = result;
        }
      } else {
        result = '@@observable';
      }

      return result;
    }

    ;
  }, {}],
  21: [function (require, module, exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var serializers_1 = require("./serializers");

    var registeredSerializer = serializers_1.DefaultSerializer;

    function registerSerializer(serializer) {
      registeredSerializer = serializers_1.extendSerializer(registeredSerializer, serializer);
    }

    exports.registerSerializer = registerSerializer;

    function deserialize(message) {
      return registeredSerializer.deserialize(message);
    }

    exports.deserialize = deserialize;

    function serialize(input) {
      return registeredSerializer.serialize(input);
    }

    exports.serialize = serialize;
  }, {
    "./serializers": 34
  }],
  22: [function (require, module, exports) {
    "use strict";

    function __export(m) {
      for (var p in m) {
        if (!exports.hasOwnProperty(p)) exports[p] = m[p];
      }
    }

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var common_1 = require("./common");

    exports.registerSerializer = common_1.registerSerializer;

    __export(require("./master/index"));

    var index_1 = require("./worker/index");

    exports.expose = index_1.expose;

    var serializers_1 = require("./serializers");

    exports.DefaultSerializer = serializers_1.DefaultSerializer;

    var transferable_1 = require("./transferable");

    exports.Transfer = transferable_1.Transfer;
  }, {
    "./common": 21,
    "./master/index": 25,
    "./serializers": 34,
    "./transferable": 36,
    "./worker/index": 40
  }],
  23: [function (require, module, exports) {
    "use strict"; // Source: <https://github.com/parcel-bundler/parcel/blob/master/packages/core/parcel-bundler/src/builtins/bundle-url.js>

    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var bundleURL;

    function getBundleURLCached() {
      if (!bundleURL) {
        bundleURL = getBundleURL();
      }

      return bundleURL;
    }

    exports.getBundleURL = getBundleURLCached;

    function getBundleURL() {
      // Attempt to find the URL of the current script and use that as the base URL
      try {
        throw new Error();
      } catch (err) {
        var matches = ("" + err.stack).match(/(https?|file|ftp|chrome-extension|moz-extension):\/\/[^)\n]+/g);

        if (matches) {
          return getBaseURL(matches[0]);
        }
      }

      return "/";
    }

    function getBaseURL(url) {
      return ("" + url).replace(/^((?:https?|file|ftp|chrome-extension|moz-extension):\/\/.+)?\/[^/]+(?:\?.*)?$/, '$1') + '/';
    }

    exports.getBaseURL = getBaseURL;
  }, {}],
  24: [function (require, module, exports) {
    "use strict"; // tslint:disable max-classes-per-file

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var get_bundle_url_browser_1 = require("./get-bundle-url.browser");

    exports.defaultPoolSize = typeof navigator !== "undefined" && navigator.hardwareConcurrency ? navigator.hardwareConcurrency : 4;

    var isAbsoluteURL = function isAbsoluteURL(value) {
      return /^(file|https?:)?\/\//i.test(value);
    };

    function createSourceBlobURL(code) {
      var blob = new Blob([code], {
        type: "application/javascript"
      });
      return URL.createObjectURL(blob);
    }

    function selectWorkerImplementation() {
      if (typeof Worker === "undefined") {
        // Might happen on Safari, for instance
        // The idea is to only fail if the constructor is actually used
        return function NoWebWorker() {
          _classCallCheck(this, NoWebWorker);

          throw Error("No web worker implementation available. You might have tried to spawn a worker within a worker in a browser that doesn't support workers in workers.");
        };
      }

      return /*#__PURE__*/function (_Worker) {
        _inherits(WebWorker, _Worker);

        var _super2 = _createSuper(WebWorker);

        function WebWorker(url, options) {
          _classCallCheck(this, WebWorker);

          if (typeof url === "string" && options && options._baseURL) {
            url = new URL(url, options._baseURL);
          } else if (typeof url === "string" && !isAbsoluteURL(url) && get_bundle_url_browser_1.getBundleURL().match(/^file:\/\//i)) {
            url = new URL(url, get_bundle_url_browser_1.getBundleURL().replace(/\/[^\/]+$/, "/"));
            url = createSourceBlobURL("importScripts(".concat(JSON.stringify(url), ");"));
          }

          if (typeof url === "string" && isAbsoluteURL(url)) {
            // Create source code blob loading JS file via `importScripts()`
            // to circumvent worker CORS restrictions
            url = createSourceBlobURL("importScripts(".concat(JSON.stringify(url), ");"));
          }

          return _super2.call(this, url, options);
        }

        return WebWorker;
      }( /*#__PURE__*/_wrapNativeSuper(Worker));
    }

    exports.selectWorkerImplementation = selectWorkerImplementation;
  }, {
    "./get-bundle-url.browser": 23
  }],
  25: [function (require, module, exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var implementation_1 = require("./implementation");

    var pool_1 = require("./pool");

    exports.Pool = pool_1.Pool;

    var spawn_1 = require("./spawn");

    exports.spawn = spawn_1.spawn;

    var thread_1 = require("./thread");

    exports.Thread = thread_1.Thread;
    /** Worker implementation. Either web worker or a node.js Worker class. */

    exports.Worker = implementation_1.selectWorkerImplementation();
  }, {
    "./implementation": 24,
    "./pool": 28,
    "./spawn": 29,
    "./thread": 30
  }],
  26: [function (require, module, exports) {
    "use strict";
    /*
     * This source file contains the code for proxying calls in the master thread to calls in the workers
     * by `.postMessage()`-ing.
     *
     * Keep in mind that this code can make or break the program's performance! Need to optimize more…
     */

    var __importDefault = this && this.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var debug_1 = __importDefault(require("debug"));

    var observable_fns_1 = require("observable-fns");

    var common_1 = require("../common");

    var observable_promise_1 = require("../observable-promise");

    var transferable_1 = require("../transferable");

    var messages_1 = require("../types/messages");

    var debugMessages = debug_1.default("threads:master:messages");
    var nextJobUID = 1;

    var dedupe = function dedupe(array) {
      return Array.from(new Set(array));
    };

    var isJobErrorMessage = function isJobErrorMessage(data) {
      return data && data.type === messages_1.WorkerMessageType.error;
    };

    var isJobResultMessage = function isJobResultMessage(data) {
      return data && data.type === messages_1.WorkerMessageType.result;
    };

    var isJobStartMessage = function isJobStartMessage(data) {
      return data && data.type === messages_1.WorkerMessageType.running;
    };

    function createObservableForJob(worker, jobUID) {
      return new observable_fns_1.Observable(function (observer) {
        var asyncType;

        var messageHandler = function messageHandler(event) {
          debugMessages("Message from worker:", event.data);
          if (!event.data || event.data.uid !== jobUID) return;

          if (isJobStartMessage(event.data)) {
            asyncType = event.data.resultType;
          } else if (isJobResultMessage(event.data)) {
            if (asyncType === "promise") {
              if (typeof event.data.payload !== "undefined") {
                observer.next(common_1.deserialize(event.data.payload));
              }

              observer.complete();
              worker.removeEventListener("message", messageHandler);
            } else {
              if (event.data.payload) {
                observer.next(common_1.deserialize(event.data.payload));
              }

              if (event.data.complete) {
                observer.complete();
                worker.removeEventListener("message", messageHandler);
              }
            }
          } else if (isJobErrorMessage(event.data)) {
            var error = common_1.deserialize(event.data.error);

            if (asyncType === "promise" || !asyncType) {
              observer.error(error);
            } else {
              observer.error(error);
            }

            worker.removeEventListener("message", messageHandler);
          }
        };

        worker.addEventListener("message", messageHandler);
        return function () {
          return worker.removeEventListener("message", messageHandler);
        };
      });
    }

    function prepareArguments(rawArgs) {
      if (rawArgs.length === 0) {
        // Exit early if possible
        return {
          args: [],
          transferables: []
        };
      }

      var args = [];
      var transferables = [];

      var _iterator9 = _createForOfIteratorHelper(rawArgs),
          _step9;

      try {
        for (_iterator9.s(); !(_step9 = _iterator9.n()).done;) {
          var arg = _step9.value;

          if (transferable_1.isTransferDescriptor(arg)) {
            args.push(common_1.serialize(arg.send));
            transferables.push.apply(transferables, _toConsumableArray(arg.transferables));
          } else {
            args.push(common_1.serialize(arg));
          }
        }
      } catch (err) {
        _iterator9.e(err);
      } finally {
        _iterator9.f();
      }

      return {
        args: args,
        transferables: transferables.length === 0 ? transferables : dedupe(transferables)
      };
    }

    function createProxyFunction(worker, method) {
      return function () {
        var uid = nextJobUID++;

        for (var _len5 = arguments.length, rawArgs = new Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {
          rawArgs[_key5] = arguments[_key5];
        }

        var _prepareArguments = prepareArguments(rawArgs),
            args = _prepareArguments.args,
            transferables = _prepareArguments.transferables;

        var runMessage = {
          type: messages_1.MasterMessageType.run,
          uid: uid,
          method: method,
          args: args
        };
        debugMessages("Sending command to run function to worker:", runMessage);

        try {
          worker.postMessage(runMessage, transferables);
        } catch (error) {
          return observable_promise_1.ObservablePromise.from(Promise.reject(error));
        }

        return observable_promise_1.ObservablePromise.from(observable_fns_1.multicast(createObservableForJob(worker, uid)));
      };
    }

    exports.createProxyFunction = createProxyFunction;

    function createProxyModule(worker, methodNames) {
      var proxy = {};

      var _iterator10 = _createForOfIteratorHelper(methodNames),
          _step10;

      try {
        for (_iterator10.s(); !(_step10 = _iterator10.n()).done;) {
          var methodName = _step10.value;
          proxy[methodName] = createProxyFunction(worker, methodName);
        }
      } catch (err) {
        _iterator10.e(err);
      } finally {
        _iterator10.f();
      }

      return proxy;
    }

    exports.createProxyModule = createProxyModule;
  }, {
    "../common": 21,
    "../observable-promise": 31,
    "../transferable": 36,
    "../types/messages": 38,
    "debug": 41,
    "observable-fns": 18
  }],
  27: [function (require, module, exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    /** Pool event type. Specifies the type of each `PoolEvent`. */

    var PoolEventType;

    (function (PoolEventType) {
      PoolEventType["initialized"] = "initialized";
      PoolEventType["taskCanceled"] = "taskCanceled";
      PoolEventType["taskCompleted"] = "taskCompleted";
      PoolEventType["taskFailed"] = "taskFailed";
      PoolEventType["taskQueued"] = "taskQueued";
      PoolEventType["taskQueueDrained"] = "taskQueueDrained";
      PoolEventType["taskStart"] = "taskStart";
      PoolEventType["terminated"] = "terminated";
    })(PoolEventType = exports.PoolEventType || (exports.PoolEventType = {}));
  }, {}],
  28: [function (require, module, exports) {
    "use strict";

    var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function (resolve) {
          resolve(value);
        });
      }

      return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }

        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }

        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }

        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };

    var __importDefault = this && this.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var debug_1 = __importDefault(require("debug"));

    var observable_fns_1 = require("observable-fns");

    var ponyfills_1 = require("../ponyfills");

    var implementation_1 = require("./implementation");

    var pool_types_1 = require("./pool-types");

    exports.PoolEventType = pool_types_1.PoolEventType;

    var thread_1 = require("./thread");

    exports.Thread = thread_1.Thread;
    var nextPoolID = 1;

    function createArray(size) {
      var array = [];

      for (var index = 0; index < size; index++) {
        array.push(index);
      }

      return array;
    }

    function delay(ms) {
      return new Promise(function (resolve) {
        return setTimeout(resolve, ms);
      });
    }

    function flatMap(array, mapper) {
      return array.reduce(function (flattened, element) {
        return [].concat(_toConsumableArray(flattened), _toConsumableArray(mapper(element)));
      }, []);
    }

    function slugify(text) {
      return text.replace(/\W/g, " ").trim().replace(/\s+/g, "-");
    }

    function spawnWorkers(spawnWorker, count) {
      return createArray(count).map(function () {
        return {
          init: spawnWorker(),
          runningTasks: []
        };
      });
    }

    var WorkerPool = /*#__PURE__*/function () {
      function WorkerPool(spawnWorker, optionsOrSize) {
        var _this15 = this;

        _classCallCheck(this, WorkerPool);

        this.eventSubject = new observable_fns_1.Subject();
        this.initErrors = [];
        this.isClosing = false;
        this.nextTaskID = 1;
        this.taskQueue = [];
        var options = typeof optionsOrSize === "number" ? {
          size: optionsOrSize
        } : optionsOrSize || {};
        var _options$size = options.size,
            size = _options$size === void 0 ? implementation_1.defaultPoolSize : _options$size;
        this.debug = debug_1.default("threads:pool:".concat(slugify(options.name || String(nextPoolID++))));
        this.options = options;
        this.workers = spawnWorkers(spawnWorker, size);
        this.eventObservable = observable_fns_1.multicast(observable_fns_1.Observable.from(this.eventSubject));
        Promise.all(this.workers.map(function (worker) {
          return worker.init;
        })).then(function () {
          return _this15.eventSubject.next({
            type: pool_types_1.PoolEventType.initialized,
            size: _this15.workers.length
          });
        }, function (error) {
          _this15.debug("Error while initializing pool worker:", error);

          _this15.eventSubject.error(error);

          _this15.initErrors.push(error);
        });
      }

      _createClass(WorkerPool, [{
        key: "findIdlingWorker",
        value: function findIdlingWorker() {
          var _this$options$concurr = this.options.concurrency,
              concurrency = _this$options$concurr === void 0 ? 1 : _this$options$concurr;
          return this.workers.find(function (worker) {
            return worker.runningTasks.length < concurrency;
          });
        }
      }, {
        key: "runPoolTask",
        value: function runPoolTask(worker, task) {
          return __awaiter(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
            var workerID, returnValue;
            return regeneratorRuntime.wrap(function _callee6$(_context6) {
              while (1) {
                switch (_context6.prev = _context6.next) {
                  case 0:
                    workerID = this.workers.indexOf(worker) + 1;
                    this.debug("Running task #".concat(task.id, " on worker #").concat(workerID, "..."));
                    this.eventSubject.next({
                      type: pool_types_1.PoolEventType.taskStart,
                      taskID: task.id,
                      workerID: workerID
                    });
                    _context6.prev = 3;
                    _context6.t0 = task;
                    _context6.next = 7;
                    return worker.init;

                  case 7:
                    _context6.t1 = _context6.sent;
                    _context6.next = 10;
                    return _context6.t0.run.call(_context6.t0, _context6.t1);

                  case 10:
                    returnValue = _context6.sent;
                    this.debug("Task #".concat(task.id, " completed successfully"));
                    this.eventSubject.next({
                      type: pool_types_1.PoolEventType.taskCompleted,
                      returnValue: returnValue,
                      taskID: task.id,
                      workerID: workerID
                    });
                    _context6.next = 19;
                    break;

                  case 15:
                    _context6.prev = 15;
                    _context6.t2 = _context6["catch"](3);
                    this.debug("Task #".concat(task.id, " failed"));
                    this.eventSubject.next({
                      type: pool_types_1.PoolEventType.taskFailed,
                      taskID: task.id,
                      error: _context6.t2,
                      workerID: workerID
                    });

                  case 19:
                  case "end":
                    return _context6.stop();
                }
              }
            }, _callee6, this, [[3, 15]]);
          }));
        }
      }, {
        key: "run",
        value: function run(worker, task) {
          return __awaiter(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
            var _this16 = this;

            var runPromise;
            return regeneratorRuntime.wrap(function _callee8$(_context8) {
              while (1) {
                switch (_context8.prev = _context8.next) {
                  case 0:
                    runPromise = function () {
                      return __awaiter(_this16, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
                        var removeTaskFromWorkersRunningTasks;
                        return regeneratorRuntime.wrap(function _callee7$(_context7) {
                          while (1) {
                            switch (_context7.prev = _context7.next) {
                              case 0:
                                removeTaskFromWorkersRunningTasks = function removeTaskFromWorkersRunningTasks() {
                                  worker.runningTasks = worker.runningTasks.filter(function (someRunPromise) {
                                    return someRunPromise !== runPromise;
                                  });
                                }; // Defer task execution by one tick to give handlers time to subscribe


                                _context7.next = 3;
                                return delay(0);

                              case 3:
                                _context7.prev = 3;
                                _context7.next = 6;
                                return this.runPoolTask(worker, task);

                              case 6:
                                _context7.prev = 6;
                                removeTaskFromWorkersRunningTasks();

                                if (!this.isClosing) {
                                  this.scheduleWork();
                                }

                                return _context7.finish(6);

                              case 10:
                              case "end":
                                return _context7.stop();
                            }
                          }
                        }, _callee7, this, [[3,, 6, 10]]);
                      }));
                    }();

                    worker.runningTasks.push(runPromise);

                  case 2:
                  case "end":
                    return _context8.stop();
                }
              }
            }, _callee8);
          }));
        }
      }, {
        key: "scheduleWork",
        value: function scheduleWork() {
          this.debug("Attempt de-queueing a task in order to run it...");
          var availableWorker = this.findIdlingWorker();
          if (!availableWorker) return;
          var nextTask = this.taskQueue.shift();

          if (!nextTask) {
            this.debug("Task queue is empty");
            this.eventSubject.next({
              type: pool_types_1.PoolEventType.taskQueueDrained
            });
            return;
          }

          this.run(availableWorker, nextTask);
        }
      }, {
        key: "taskCompletion",
        value: function taskCompletion(taskID) {
          var _this17 = this;

          return new Promise(function (resolve, reject) {
            var eventSubscription = _this17.events().subscribe(function (event) {
              if (event.type === pool_types_1.PoolEventType.taskCompleted && event.taskID === taskID) {
                eventSubscription.unsubscribe();
                resolve(event.returnValue);
              } else if (event.type === pool_types_1.PoolEventType.taskFailed && event.taskID === taskID) {
                eventSubscription.unsubscribe();
                reject(event.error);
              } else if (event.type === pool_types_1.PoolEventType.terminated) {
                eventSubscription.unsubscribe();
                reject(Error("Pool has been terminated before task was run."));
              }
            });
          });
        }
      }, {
        key: "settled",
        value: function settled() {
          var allowResolvingImmediately = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
          return __awaiter(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
            var _this18 = this;

            var getCurrentlyRunningTasks, taskFailures, failureSubscription;
            return regeneratorRuntime.wrap(function _callee9$(_context9) {
              while (1) {
                switch (_context9.prev = _context9.next) {
                  case 0:
                    getCurrentlyRunningTasks = function getCurrentlyRunningTasks() {
                      return flatMap(_this18.workers, function (worker) {
                        return worker.runningTasks;
                      });
                    };

                    taskFailures = [];
                    failureSubscription = this.eventObservable.subscribe(function (event) {
                      if (event.type === pool_types_1.PoolEventType.taskFailed) {
                        taskFailures.push(event.error);
                      }
                    });

                    if (!(this.initErrors.length > 0)) {
                      _context9.next = 5;
                      break;
                    }

                    return _context9.abrupt("return", Promise.reject(this.initErrors[0]));

                  case 5:
                    if (!(allowResolvingImmediately && this.taskQueue.length === 0)) {
                      _context9.next = 9;
                      break;
                    }

                    _context9.next = 8;
                    return ponyfills_1.allSettled(getCurrentlyRunningTasks());

                  case 8:
                    return _context9.abrupt("return", taskFailures);

                  case 9:
                    _context9.next = 11;
                    return new Promise(function (resolve, reject) {
                      var subscription = _this18.eventObservable.subscribe({
                        next: function next(event) {
                          if (event.type === pool_types_1.PoolEventType.taskQueueDrained) {
                            subscription.unsubscribe();
                            resolve();
                          }
                        },
                        error: reject // make a pool-wide error reject the completed() result promise

                      });
                    });

                  case 11:
                    _context9.next = 13;
                    return ponyfills_1.allSettled(getCurrentlyRunningTasks());

                  case 13:
                    failureSubscription.unsubscribe();
                    return _context9.abrupt("return", taskFailures);

                  case 15:
                  case "end":
                    return _context9.stop();
                }
              }
            }, _callee9, this);
          }));
        }
      }, {
        key: "completed",
        value: function completed() {
          var allowResolvingImmediately = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
          return __awaiter(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee10() {
            var _this19 = this;

            var settlementPromise, earlyExitPromise, errors;
            return regeneratorRuntime.wrap(function _callee10$(_context10) {
              while (1) {
                switch (_context10.prev = _context10.next) {
                  case 0:
                    settlementPromise = this.settled(allowResolvingImmediately);
                    earlyExitPromise = new Promise(function (resolve, reject) {
                      var subscription = _this19.eventObservable.subscribe({
                        next: function next(event) {
                          if (event.type === pool_types_1.PoolEventType.taskQueueDrained) {
                            subscription.unsubscribe();
                            resolve(settlementPromise);
                          } else if (event.type === pool_types_1.PoolEventType.taskFailed) {
                            subscription.unsubscribe();
                            reject(event.error);
                          }
                        },
                        error: reject // make a pool-wide error reject the completed() result promise

                      });
                    });
                    _context10.next = 4;
                    return Promise.race([settlementPromise, earlyExitPromise]);

                  case 4:
                    errors = _context10.sent;

                    if (!(errors.length > 0)) {
                      _context10.next = 7;
                      break;
                    }

                    throw errors[0];

                  case 7:
                  case "end":
                    return _context10.stop();
                }
              }
            }, _callee10, this);
          }));
        }
      }, {
        key: "events",
        value: function events() {
          return this.eventObservable;
        }
      }, {
        key: "queue",
        value: function queue(taskFunction) {
          var _this20 = this;

          var _this$options$maxQueu = this.options.maxQueuedJobs,
              maxQueuedJobs = _this$options$maxQueu === void 0 ? Infinity : _this$options$maxQueu;

          if (this.isClosing) {
            throw Error("Cannot schedule pool tasks after terminate() has been called.");
          }

          if (this.initErrors.length > 0) {
            throw this.initErrors[0];
          }

          var taskCompleted = function taskCompleted() {
            return _this20.taskCompletion(task.id);
          };

          var taskCompletionDotThen;
          var task = {
            id: this.nextTaskID++,
            run: taskFunction,
            cancel: function cancel() {
              if (_this20.taskQueue.indexOf(task) === -1) return;
              _this20.taskQueue = _this20.taskQueue.filter(function (someTask) {
                return someTask !== task;
              });

              _this20.eventSubject.next({
                type: pool_types_1.PoolEventType.taskCanceled,
                taskID: task.id
              });
            },

            get then() {
              if (!taskCompletionDotThen) {
                var promise = taskCompleted();
                taskCompletionDotThen = promise.then.bind(promise);
              }

              return taskCompletionDotThen;
            }

          };

          if (this.taskQueue.length >= maxQueuedJobs) {
            throw Error("Maximum number of pool tasks queued. Refusing to queue another one.\n" + "This usually happens for one of two reasons: We are either at peak " + "workload right now or some tasks just won't finish, thus blocking the pool.");
          }

          this.debug("Queueing task #".concat(task.id, "..."));
          this.taskQueue.push(task);
          this.eventSubject.next({
            type: pool_types_1.PoolEventType.taskQueued,
            taskID: task.id
          });
          this.scheduleWork();
          return task;
        }
      }, {
        key: "terminate",
        value: function terminate(force) {
          return __awaiter(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee12() {
            var _this21 = this;

            return regeneratorRuntime.wrap(function _callee12$(_context12) {
              while (1) {
                switch (_context12.prev = _context12.next) {
                  case 0:
                    this.isClosing = true;

                    if (force) {
                      _context12.next = 4;
                      break;
                    }

                    _context12.next = 4;
                    return this.completed(true);

                  case 4:
                    this.eventSubject.next({
                      type: pool_types_1.PoolEventType.terminated,
                      remainingQueue: _toConsumableArray(this.taskQueue)
                    });
                    this.eventSubject.complete();
                    _context12.next = 8;
                    return Promise.all(this.workers.map(function (worker) {
                      return __awaiter(_this21, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee11() {
                        return regeneratorRuntime.wrap(function _callee11$(_context11) {
                          while (1) {
                            switch (_context11.prev = _context11.next) {
                              case 0:
                                _context11.t0 = thread_1.Thread;
                                _context11.next = 3;
                                return worker.init;

                              case 3:
                                _context11.t1 = _context11.sent;
                                return _context11.abrupt("return", _context11.t0.terminate.call(_context11.t0, _context11.t1));

                              case 5:
                              case "end":
                                return _context11.stop();
                            }
                          }
                        }, _callee11);
                      }));
                    }));

                  case 8:
                  case "end":
                    return _context12.stop();
                }
              }
            }, _callee12, this);
          }));
        }
      }]);

      return WorkerPool;
    }();

    WorkerPool.EventType = pool_types_1.PoolEventType;
    /**
     * Thread pool constructor. Creates a new pool and spawns its worker threads.
     */

    function PoolConstructor(spawnWorker, optionsOrSize) {
      // The function exists only so we don't need to use `new` to create a pool (we still can, though).
      // If the Pool is a class or not is an implementation detail that should not concern the user.
      return new WorkerPool(spawnWorker, optionsOrSize);
    }

    PoolConstructor.EventType = pool_types_1.PoolEventType;
    /**
     * Thread pool constructor. Creates a new pool and spawns its worker threads.
     */

    exports.Pool = PoolConstructor;
  }, {
    "../ponyfills": 32,
    "./implementation": 24,
    "./pool-types": 27,
    "./thread": 30,
    "debug": 41,
    "observable-fns": 18
  }],
  29: [function (require, module, exports) {
    (function (process) {
      "use strict";

      var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
        function adopt(value) {
          return value instanceof P ? value : new P(function (resolve) {
            resolve(value);
          });
        }

        return new (P || (P = Promise))(function (resolve, reject) {
          function fulfilled(value) {
            try {
              step(generator.next(value));
            } catch (e) {
              reject(e);
            }
          }

          function rejected(value) {
            try {
              step(generator["throw"](value));
            } catch (e) {
              reject(e);
            }
          }

          function step(result) {
            result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
          }

          step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
      };

      var __importDefault = this && this.__importDefault || function (mod) {
        return mod && mod.__esModule ? mod : {
          "default": mod
        };
      };

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var debug_1 = __importDefault(require("debug"));

      var observable_fns_1 = require("observable-fns");

      var common_1 = require("../common");

      var promise_1 = require("../promise");

      var symbols_1 = require("../symbols");

      var master_1 = require("../types/master");

      var invocation_proxy_1 = require("./invocation-proxy");

      var debugMessages = debug_1.default("threads:master:messages");
      var debugSpawn = debug_1.default("threads:master:spawn");
      var debugThreadUtils = debug_1.default("threads:master:thread-utils");

      var isInitMessage = function isInitMessage(data) {
        return data && data.type === "init";
      };

      var isUncaughtErrorMessage = function isUncaughtErrorMessage(data) {
        return data && data.type === "uncaughtError";
      };

      var initMessageTimeout = typeof process !== "undefined" && undefined ? Number.parseInt(undefined, 10) : 10000;

      function withTimeout(promise, timeoutInMs, errorMessage) {
        return __awaiter(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee13() {
          var timeoutHandle, timeout, result;
          return regeneratorRuntime.wrap(function _callee13$(_context13) {
            while (1) {
              switch (_context13.prev = _context13.next) {
                case 0:
                  timeout = new Promise(function (resolve, reject) {
                    timeoutHandle = setTimeout(function () {
                      return reject(Error(errorMessage));
                    }, timeoutInMs);
                  });
                  _context13.next = 3;
                  return Promise.race([promise, timeout]);

                case 3:
                  result = _context13.sent;
                  clearTimeout(timeoutHandle);
                  return _context13.abrupt("return", result);

                case 6:
                case "end":
                  return _context13.stop();
              }
            }
          }, _callee13);
        }));
      }

      function receiveInitMessage(worker) {
        return new Promise(function (resolve, reject) {
          var messageHandler = function messageHandler(event) {
            debugMessages("Message from worker before finishing initialization:", event.data);

            if (isInitMessage(event.data)) {
              worker.removeEventListener("message", messageHandler);
              resolve(event.data);
            } else if (isUncaughtErrorMessage(event.data)) {
              worker.removeEventListener("message", messageHandler);
              reject(common_1.deserialize(event.data.error));
            }
          };

          worker.addEventListener("message", messageHandler);
        });
      }

      function createEventObservable(worker, workerTermination) {
        return new observable_fns_1.Observable(function (observer) {
          var messageHandler = function messageHandler(messageEvent) {
            var workerEvent = {
              type: master_1.WorkerEventType.message,
              data: messageEvent.data
            };
            observer.next(workerEvent);
          };

          var rejectionHandler = function rejectionHandler(errorEvent) {
            debugThreadUtils("Unhandled promise rejection event in thread:", errorEvent);
            var workerEvent = {
              type: master_1.WorkerEventType.internalError,
              error: Error(errorEvent.reason)
            };
            observer.next(workerEvent);
          };

          worker.addEventListener("message", messageHandler);
          worker.addEventListener("unhandledrejection", rejectionHandler);
          workerTermination.then(function () {
            var terminationEvent = {
              type: master_1.WorkerEventType.termination
            };
            worker.removeEventListener("message", messageHandler);
            worker.removeEventListener("unhandledrejection", rejectionHandler);
            observer.next(terminationEvent);
            observer.complete();
          });
        });
      }

      function createTerminator(worker) {
        var _this22 = this;

        var _promise_1$createProm = promise_1.createPromiseWithResolver(),
            _promise_1$createProm2 = _slicedToArray(_promise_1$createProm, 2),
            termination = _promise_1$createProm2[0],
            resolver = _promise_1$createProm2[1];

        var terminate = function terminate() {
          return __awaiter(_this22, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee14() {
            return regeneratorRuntime.wrap(function _callee14$(_context14) {
              while (1) {
                switch (_context14.prev = _context14.next) {
                  case 0:
                    debugThreadUtils("Terminating worker"); // Newer versions of worker_threads workers return a promise

                    _context14.next = 3;
                    return worker.terminate();

                  case 3:
                    resolver();

                  case 4:
                  case "end":
                    return _context14.stop();
                }
              }
            }, _callee14);
          }));
        };

        return {
          terminate: terminate,
          termination: termination
        };
      }

      function setPrivateThreadProps(raw, worker, workerEvents, terminate) {
        var _Object$assign;

        var workerErrors = workerEvents.filter(function (event) {
          return event.type === master_1.WorkerEventType.internalError;
        }).map(function (errorEvent) {
          return errorEvent.error;
        }); // tslint:disable-next-line prefer-object-spread

        return Object.assign(raw, (_Object$assign = {}, _defineProperty(_Object$assign, symbols_1.$errors, workerErrors), _defineProperty(_Object$assign, symbols_1.$events, workerEvents), _defineProperty(_Object$assign, symbols_1.$terminate, terminate), _defineProperty(_Object$assign, symbols_1.$worker, worker), _Object$assign));
      }
      /**
       * Spawn a new thread. Takes a fresh worker instance, wraps it in a thin
       * abstraction layer to provide the transparent API and verifies that
       * the worker has initialized successfully.
       *
       * @param worker Instance of `Worker`. Either a web worker, `worker_threads` worker or `tiny-worker` worker.
       * @param [options]
       * @param [options.timeout] Init message timeout. Default: 10000 or set by environment variable.
       */


      function spawn(worker, options) {
        return __awaiter(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee15() {
          var initMessage, exposed, _createTerminator, termination, terminate, events, proxy, _proxy, type;

          return regeneratorRuntime.wrap(function _callee15$(_context15) {
            while (1) {
              switch (_context15.prev = _context15.next) {
                case 0:
                  debugSpawn("Initializing new thread");
                  _context15.next = 3;
                  return withTimeout(receiveInitMessage(worker), options && options.timeout ? options.timeout : initMessageTimeout, "Timeout: Did not receive an init message from worker after ".concat(initMessageTimeout, "ms. Make sure the worker calls expose()."));

                case 3:
                  initMessage = _context15.sent;
                  exposed = initMessage.exposed;
                  _createTerminator = createTerminator(worker), termination = _createTerminator.termination, terminate = _createTerminator.terminate;
                  events = createEventObservable(worker, termination);

                  if (!(exposed.type === "function")) {
                    _context15.next = 12;
                    break;
                  }

                  proxy = invocation_proxy_1.createProxyFunction(worker);
                  return _context15.abrupt("return", setPrivateThreadProps(proxy, worker, events, terminate));

                case 12:
                  if (!(exposed.type === "module")) {
                    _context15.next = 17;
                    break;
                  }

                  _proxy = invocation_proxy_1.createProxyModule(worker, exposed.methods);
                  return _context15.abrupt("return", setPrivateThreadProps(_proxy, worker, events, terminate));

                case 17:
                  type = exposed.type;
                  throw Error("Worker init message states unexpected type of expose(): ".concat(type));

                case 19:
                case "end":
                  return _context15.stop();
              }
            }
          }, _callee15);
        }));
      }

      exports.spawn = spawn;
    }).call(this, require('_process'));
  }, {
    "../common": 21,
    "../promise": 33,
    "../symbols": 35,
    "../types/master": 37,
    "./invocation-proxy": 26,
    "_process": 43,
    "debug": 41,
    "observable-fns": 18
  }],
  30: [function (require, module, exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var symbols_1 = require("../symbols");

    function fail(message) {
      throw Error(message);
    }
    /** Thread utility functions. Use them to manage or inspect a `spawn()`-ed thread. */


    exports.Thread = {
      /** Return an observable that can be used to subscribe to all errors happening in the thread. */
      errors: function errors(thread) {
        return thread[symbols_1.$errors] || fail("Error observable not found. Make sure to pass a thread instance as returned by the spawn() promise.");
      },

      /** Return an observable that can be used to subscribe to internal events happening in the thread. Useful for debugging. */
      events: function events(thread) {
        return thread[symbols_1.$events] || fail("Events observable not found. Make sure to pass a thread instance as returned by the spawn() promise.");
      },

      /** Terminate a thread. Remember to terminate every thread when you are done using it. */
      terminate: function terminate(thread) {
        return thread[symbols_1.$terminate]();
      }
    };
  }, {
    "../symbols": 35
  }],
  31: [function (require, module, exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var observable_fns_1 = require("observable-fns");

    var doNothing = function doNothing() {
      return undefined;
    };

    var returnInput = function returnInput(input) {
      return input;
    };

    var runDeferred = function runDeferred(fn) {
      return Promise.resolve().then(fn);
    };

    function fail(error) {
      throw error;
    }

    function isThenable(thing) {
      return thing && typeof thing.then === "function";
    }
    /**
     * Creates a hybrid, combining the APIs of an Observable and a Promise.
     *
     * It is used to proxy async process states when we are initially not sure
     * if that async process will yield values once (-> Promise) or multiple
     * times (-> Observable).
     *
     * Note that the observable promise inherits some of the observable's characteristics:
     * The `init` function will be called *once for every time anyone subscribes to it*.
     *
     * If this is undesired, derive a hot observable from it using `makeHot()` and
     * subscribe to that.
     */


    var ObservablePromise = /*#__PURE__*/function (_observable_fns_1$Obs) {
      _inherits(ObservablePromise, _observable_fns_1$Obs);

      var _super3 = _createSuper(ObservablePromise);

      function ObservablePromise(init) {
        var _this23;

        _classCallCheck(this, ObservablePromise);

        _this23 = _super3.call(this, function (originalObserver) {
          // tslint:disable-next-line no-this-assignment
          var self = _assertThisInitialized(_this23);

          var observer = Object.assign(Object.assign({}, originalObserver), {
            complete: function complete() {
              originalObserver.complete();
              self.onCompletion();
            },
            error: function error(_error9) {
              originalObserver.error(_error9);
              self.onError(_error9);
            },
            next: function next(value) {
              originalObserver.next(value);
              self.onNext(value);
            }
          });

          try {
            _this23.initHasRun = true;
            return init(observer);
          } catch (error) {
            observer.error(error);
          }
        });
        _this23.initHasRun = false;
        _this23.fulfillmentCallbacks = [];
        _this23.rejectionCallbacks = [];
        _this23.firstValueSet = false;
        _this23.state = "pending";
        return _this23;
      }

      _createClass(ObservablePromise, [{
        key: "onNext",
        value: function onNext(value) {
          if (!this.firstValueSet) {
            this.firstValue = value;
            this.firstValueSet = true;
          }
        }
      }, {
        key: "onError",
        value: function onError(error) {
          this.state = "rejected";
          this.rejection = error;

          var _iterator11 = _createForOfIteratorHelper(this.rejectionCallbacks),
              _step11;

          try {
            var _loop = function _loop() {
              var onRejected = _step11.value;
              // Promisifying the call to turn errors into unhandled promise rejections
              // instead of them failing sync and cancelling the iteration
              runDeferred(function () {
                return onRejected(error);
              });
            };

            for (_iterator11.s(); !(_step11 = _iterator11.n()).done;) {
              _loop();
            }
          } catch (err) {
            _iterator11.e(err);
          } finally {
            _iterator11.f();
          }
        }
      }, {
        key: "onCompletion",
        value: function onCompletion() {
          var _this24 = this;

          this.state = "fulfilled";

          var _iterator12 = _createForOfIteratorHelper(this.fulfillmentCallbacks),
              _step12;

          try {
            var _loop2 = function _loop2() {
              var onFulfilled = _step12.value;
              // Promisifying the call to turn errors into unhandled promise rejections
              // instead of them failing sync and cancelling the iteration
              runDeferred(function () {
                return onFulfilled(_this24.firstValue);
              });
            };

            for (_iterator12.s(); !(_step12 = _iterator12.n()).done;) {
              _loop2();
            }
          } catch (err) {
            _iterator12.e(err);
          } finally {
            _iterator12.f();
          }
        }
      }, {
        key: "then",
        value: function then(onFulfilledRaw, onRejectedRaw) {
          var _this25 = this;

          var onFulfilled = onFulfilledRaw || returnInput;
          var onRejected = onRejectedRaw || fail;
          var onRejectedCalled = false;
          return new Promise(function (resolve, reject) {
            var rejectionCallback = function rejectionCallback(error) {
              if (onRejectedCalled) return;
              onRejectedCalled = true;

              try {
                resolve(onRejected(error));
              } catch (anotherError) {
                reject(anotherError);
              }
            };

            var fulfillmentCallback = function fulfillmentCallback(value) {
              try {
                resolve(onFulfilled(value));
              } catch (error) {
                rejectionCallback(error);
              }
            };

            if (!_this25.initHasRun) {
              _this25.subscribe({
                error: rejectionCallback
              });
            }

            if (_this25.state === "fulfilled") {
              return resolve(onFulfilled(_this25.firstValue));
            }

            if (_this25.state === "rejected") {
              onRejectedCalled = true;
              return resolve(onRejected(_this25.rejection));
            }

            _this25.fulfillmentCallbacks.push(fulfillmentCallback);

            _this25.rejectionCallbacks.push(rejectionCallback);
          });
        }
      }, {
        key: "catch",
        value: function _catch(onRejected) {
          return this.then(undefined, onRejected);
        }
      }, {
        key: "finally",
        value: function _finally(onCompleted) {
          var handler = onCompleted || doNothing;
          return this.then(function (value) {
            handler();
            return value;
          }, function () {
            return handler();
          });
        }
      }], [{
        key: "from",
        value: function from(thing) {
          if (isThenable(thing)) {
            return new ObservablePromise(function (observer) {
              var onFulfilled = function onFulfilled(value) {
                observer.next(value);
                observer.complete();
              };

              var onRejected = function onRejected(error) {
                observer.error(error);
              };

              thing.then(onFulfilled, onRejected);
            });
          } else {
            return _get(_getPrototypeOf(ObservablePromise), "from", this).call(this, thing);
          }
        }
      }]);

      return ObservablePromise;
    }(observable_fns_1.Observable);

    exports.ObservablePromise = ObservablePromise;
  }, {
    "observable-fns": 18
  }],
  32: [function (require, module, exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    }); // Based on <https://github.com/es-shims/Promise.allSettled/blob/master/implementation.js>

    function allSettled(values) {
      return Promise.all(values.map(function (item) {
        var onFulfill = function onFulfill(value) {
          return {
            status: 'fulfilled',
            value: value
          };
        };

        var onReject = function onReject(reason) {
          return {
            status: 'rejected',
            reason: reason
          };
        };

        var itemPromise = Promise.resolve(item);

        try {
          return itemPromise.then(onFulfill, onReject);
        } catch (error) {
          return Promise.reject(error);
        }
      }));
    }

    exports.allSettled = allSettled;
  }, {}],
  33: [function (require, module, exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var doNothing = function doNothing() {
      return undefined;
    };
    /**
     * Creates a new promise and exposes its resolver function.
     * Use with care!
     */


    function createPromiseWithResolver() {
      var alreadyResolved = false;
      var resolvedTo;
      var resolver = doNothing;
      var promise = new Promise(function (resolve) {
        if (alreadyResolved) {
          resolve(resolvedTo);
        } else {
          resolver = resolve;
        }
      });

      var exposedResolver = function exposedResolver(value) {
        alreadyResolved = true;
        resolvedTo = value;
        resolver();
      };

      return [promise, exposedResolver];
    }

    exports.createPromiseWithResolver = createPromiseWithResolver;
  }, {}],
  34: [function (require, module, exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    function extendSerializer(extend, implementation) {
      var fallbackDeserializer = extend.deserialize.bind(extend);
      var fallbackSerializer = extend.serialize.bind(extend);
      return {
        deserialize: function deserialize(message) {
          return implementation.deserialize(message, fallbackDeserializer);
        },
        serialize: function serialize(input) {
          return implementation.serialize(input, fallbackSerializer);
        }
      };
    }

    exports.extendSerializer = extendSerializer;
    var DefaultErrorSerializer = {
      deserialize: function deserialize(message) {
        return Object.assign(Error(message.message), {
          name: message.name,
          stack: message.stack
        });
      },
      serialize: function serialize(error) {
        return {
          __error_marker: "$$error",
          message: error.message,
          name: error.name,
          stack: error.stack
        };
      }
    };

    var isSerializedError = function isSerializedError(thing) {
      return thing && _typeof(thing) === "object" && "__error_marker" in thing && thing.__error_marker === "$$error";
    };

    exports.DefaultSerializer = {
      deserialize: function deserialize(message) {
        if (isSerializedError(message)) {
          return DefaultErrorSerializer.deserialize(message);
        } else {
          return message;
        }
      },
      serialize: function serialize(input) {
        if (input instanceof Error) {
          return DefaultErrorSerializer.serialize(input);
        } else {
          return input;
        }
      }
    };
  }, {}],
  35: [function (require, module, exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.$errors = Symbol("thread.errors");
    exports.$events = Symbol("thread.events");
    exports.$terminate = Symbol("thread.terminate");
    exports.$transferable = Symbol("thread.transferable");
    exports.$worker = Symbol("thread.worker");
  }, {}],
  36: [function (require, module, exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var symbols_1 = require("./symbols");

    function isTransferable(thing) {
      if (!thing || _typeof(thing) !== "object") return false; // Don't check too thoroughly, since the list of transferable things in JS might grow over time

      return true;
    }

    function isTransferDescriptor(thing) {
      return thing && _typeof(thing) === "object" && thing[symbols_1.$transferable];
    }

    exports.isTransferDescriptor = isTransferDescriptor;

    function Transfer(payload, transferables) {
      var _ref;

      if (!transferables) {
        if (!isTransferable(payload)) throw Error();
        transferables = [payload];
      }

      return _ref = {}, _defineProperty(_ref, symbols_1.$transferable, true), _defineProperty(_ref, "send", payload), _defineProperty(_ref, "transferables", transferables), _ref;
    }

    exports.Transfer = Transfer;
  }, {
    "./symbols": 35
  }],
  37: [function (require, module, exports) {
    "use strict"; /// <reference lib="dom" />

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var symbols_1 = require("../symbols");
    /** Event as emitted by worker thread. Subscribe to using `Thread.events(thread)`. */


    var WorkerEventType;

    (function (WorkerEventType) {
      WorkerEventType["internalError"] = "internalError";
      WorkerEventType["message"] = "message";
      WorkerEventType["termination"] = "termination";
    })(WorkerEventType = exports.WorkerEventType || (exports.WorkerEventType = {}));
  }, {
    "../symbols": 35
  }],
  38: [function (require, module, exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    }); /////////////////////////////
    // Messages sent by master:

    var MasterMessageType;

    (function (MasterMessageType) {
      MasterMessageType["run"] = "run";
    })(MasterMessageType = exports.MasterMessageType || (exports.MasterMessageType = {})); ////////////////////////////
    // Messages sent by worker:


    var WorkerMessageType;

    (function (WorkerMessageType) {
      WorkerMessageType["error"] = "error";
      WorkerMessageType["init"] = "init";
      WorkerMessageType["result"] = "result";
      WorkerMessageType["running"] = "running";
      WorkerMessageType["uncaughtError"] = "uncaughtError";
    })(WorkerMessageType = exports.WorkerMessageType || (exports.WorkerMessageType = {}));
  }, {}],
  39: [function (require, module, exports) {
    "use strict"; /// <reference lib="dom" />
    // tslint:disable no-shadowed-variable

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var isWorkerRuntime = function isWorkerRuntime() {
      return typeof self !== "undefined" && self.postMessage ? true : false;
    };

    var postMessageToMaster = function postMessageToMaster(data, transferList) {
      self.postMessage(data, transferList);
    };

    var subscribeToMasterMessages = function subscribeToMasterMessages(onMessage) {
      var messageHandler = function messageHandler(messageEvent) {
        onMessage(messageEvent.data);
      };

      var unsubscribe = function unsubscribe() {
        self.removeEventListener("message", messageHandler);
      };

      self.addEventListener("message", messageHandler);
      return unsubscribe;
    };

    exports.default = {
      isWorkerRuntime: isWorkerRuntime,
      postMessageToMaster: postMessageToMaster,
      subscribeToMasterMessages: subscribeToMasterMessages
    };
  }, {}],
  40: [function (require, module, exports) {
    (function (process) {
      "use strict";

      var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
        function adopt(value) {
          return value instanceof P ? value : new P(function (resolve) {
            resolve(value);
          });
        }

        return new (P || (P = Promise))(function (resolve, reject) {
          function fulfilled(value) {
            try {
              step(generator.next(value));
            } catch (e) {
              reject(e);
            }
          }

          function rejected(value) {
            try {
              step(generator["throw"](value));
            } catch (e) {
              reject(e);
            }
          }

          function step(result) {
            result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
          }

          step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
      };

      var __importDefault = this && this.__importDefault || function (mod) {
        return mod && mod.__esModule ? mod : {
          "default": mod
        };
      };

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var is_observable_1 = __importDefault(require("is-observable"));

      var common_1 = require("../common");

      var transferable_1 = require("../transferable");

      var messages_1 = require("../types/messages");

      var implementation_1 = __importDefault(require("./implementation"));

      var common_2 = require("../common");

      exports.registerSerializer = common_2.registerSerializer;

      var transferable_2 = require("../transferable");

      exports.Transfer = transferable_2.Transfer;
      var exposeCalled = false;

      var isMasterJobRunMessage = function isMasterJobRunMessage(thing) {
        return thing && thing.type === messages_1.MasterMessageType.run;
      };
      /**
       * There are issues with `is-observable` not recognizing zen-observable's instances.
       * We are using `observable-fns`, but it's based on zen-observable, too.
       */


      var isObservable = function isObservable(thing) {
        return is_observable_1.default(thing) || isZenObservable(thing);
      };

      function isZenObservable(thing) {
        return thing && _typeof(thing) === "object" && typeof thing.subscribe === "function";
      }

      function deconstructTransfer(thing) {
        return transferable_1.isTransferDescriptor(thing) ? {
          payload: thing.send,
          transferables: thing.transferables
        } : {
          payload: thing,
          transferables: undefined
        };
      }

      function postFunctionInitMessage() {
        var initMessage = {
          type: messages_1.WorkerMessageType.init,
          exposed: {
            type: "function"
          }
        };
        implementation_1.default.postMessageToMaster(initMessage);
      }

      function postModuleInitMessage(methodNames) {
        var initMessage = {
          type: messages_1.WorkerMessageType.init,
          exposed: {
            type: "module",
            methods: methodNames
          }
        };
        implementation_1.default.postMessageToMaster(initMessage);
      }

      function postJobErrorMessage(uid, rawError) {
        var _deconstructTransfer = deconstructTransfer(rawError),
            error = _deconstructTransfer.payload,
            transferables = _deconstructTransfer.transferables;

        var errorMessage = {
          type: messages_1.WorkerMessageType.error,
          uid: uid,
          error: common_1.serialize(error)
        };
        implementation_1.default.postMessageToMaster(errorMessage, transferables);
      }

      function postJobResultMessage(uid, completed, resultValue) {
        var _deconstructTransfer2 = deconstructTransfer(resultValue),
            payload = _deconstructTransfer2.payload,
            transferables = _deconstructTransfer2.transferables;

        var resultMessage = {
          type: messages_1.WorkerMessageType.result,
          uid: uid,
          complete: completed ? true : undefined,
          payload: payload
        };
        implementation_1.default.postMessageToMaster(resultMessage, transferables);
      }

      function postJobStartMessage(uid, resultType) {
        var startMessage = {
          type: messages_1.WorkerMessageType.running,
          uid: uid,
          resultType: resultType
        };
        implementation_1.default.postMessageToMaster(startMessage);
      }

      function postUncaughtErrorMessage(error) {
        try {
          var errorMessage = {
            type: messages_1.WorkerMessageType.uncaughtError,
            error: common_1.serialize(error)
          };
          implementation_1.default.postMessageToMaster(errorMessage);
        } catch (subError) {
          // tslint:disable-next-line no-console
          console.error("Not reporting uncaught error back to master thread as it " + "occured while reporting an uncaught error already." + "\nLatest error:", subError, "\nOriginal error:", error);
        }
      }

      function runFunction(jobUID, fn, args) {
        return __awaiter(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee16() {
          var syncResult, resultType, result;
          return regeneratorRuntime.wrap(function _callee16$(_context16) {
            while (1) {
              switch (_context16.prev = _context16.next) {
                case 0:
                  _context16.prev = 0;
                  syncResult = fn.apply(void 0, _toConsumableArray(args));
                  _context16.next = 7;
                  break;

                case 4:
                  _context16.prev = 4;
                  _context16.t0 = _context16["catch"](0);
                  return _context16.abrupt("return", postJobErrorMessage(jobUID, _context16.t0));

                case 7:
                  resultType = isObservable(syncResult) ? "observable" : "promise";
                  postJobStartMessage(jobUID, resultType);

                  if (!isObservable(syncResult)) {
                    _context16.next = 13;
                    break;
                  }

                  syncResult.subscribe(function (value) {
                    return postJobResultMessage(jobUID, false, common_1.serialize(value));
                  }, function (error) {
                    return postJobErrorMessage(jobUID, common_1.serialize(error));
                  }, function () {
                    return postJobResultMessage(jobUID, true);
                  });
                  _context16.next = 23;
                  break;

                case 13:
                  _context16.prev = 13;
                  _context16.next = 16;
                  return syncResult;

                case 16:
                  result = _context16.sent;
                  postJobResultMessage(jobUID, true, common_1.serialize(result));
                  _context16.next = 23;
                  break;

                case 20:
                  _context16.prev = 20;
                  _context16.t1 = _context16["catch"](13);
                  postJobErrorMessage(jobUID, common_1.serialize(_context16.t1));

                case 23:
                case "end":
                  return _context16.stop();
              }
            }
          }, _callee16, null, [[0, 4], [13, 20]]);
        }));
      }
      /**
       * Expose a function or a module (an object whose values are functions)
       * to the main thread. Must be called exactly once in every worker thread
       * to signal its API to the main thread.
       *
       * @param exposed Function or object whose values are functions
       */


      function expose(exposed) {
        if (!implementation_1.default.isWorkerRuntime()) {
          throw Error("expose() called in the master thread.");
        }

        if (exposeCalled) {
          throw Error("expose() called more than once. This is not possible. Pass an object to expose() if you want to expose multiple functions.");
        }

        exposeCalled = true;

        if (typeof exposed === "function") {
          implementation_1.default.subscribeToMasterMessages(function (messageData) {
            if (isMasterJobRunMessage(messageData) && !messageData.method) {
              runFunction(messageData.uid, exposed, messageData.args.map(common_1.deserialize));
            }
          });
          postFunctionInitMessage();
        } else if (_typeof(exposed) === "object" && exposed) {
          implementation_1.default.subscribeToMasterMessages(function (messageData) {
            if (isMasterJobRunMessage(messageData) && messageData.method) {
              runFunction(messageData.uid, exposed[messageData.method], messageData.args.map(common_1.deserialize));
            }
          });
          var methodNames = Object.keys(exposed).filter(function (key) {
            return typeof exposed[key] === "function";
          });
          postModuleInitMessage(methodNames);
        } else {
          throw Error("Invalid argument passed to expose(). Expected a function or an object, got: ".concat(exposed));
        }
      }

      exports.expose = expose;

      if (typeof self !== "undefined" && typeof self.addEventListener === "function" && implementation_1.default.isWorkerRuntime()) {
        self.addEventListener("error", function (event) {
          // Post with some delay, so the master had some time to subscribe to messages
          setTimeout(function () {
            return postUncaughtErrorMessage(event.error || event);
          }, 250);
        });
        self.addEventListener("unhandledrejection", function (event) {
          var error = event.reason;

          if (error && typeof error.message === "string") {
            // Post with some delay, so the master had some time to subscribe to messages
            setTimeout(function () {
              return postUncaughtErrorMessage(error);
            }, 250);
          }
        });
      }

      if (typeof process !== "undefined" && typeof process.on === "function" && implementation_1.default.isWorkerRuntime()) {
        process.on("uncaughtException", function (error) {
          // Post with some delay, so the master had some time to subscribe to messages
          setTimeout(function () {
            return postUncaughtErrorMessage(error);
          }, 250);
        });
        process.on("unhandledRejection", function (error) {
          if (error && typeof error.message === "string") {
            // Post with some delay, so the master had some time to subscribe to messages
            setTimeout(function () {
              return postUncaughtErrorMessage(error);
            }, 250);
          }
        });
      }
    }).call(this, require('_process'));
  }, {
    "../common": 21,
    "../transferable": 36,
    "../types/messages": 38,
    "./implementation": 39,
    "_process": 43,
    "is-observable": 2
  }],
  41: [function (require, module, exports) {
    (function (process) {
      /* eslint-env browser */

      /**
       * This is the web browser implementation of `debug()`.
       */
      exports.log = log;
      exports.formatArgs = formatArgs;
      exports.save = save;
      exports.load = load;
      exports.useColors = useColors;
      exports.storage = localstorage();
      /**
       * Colors.
       */

      exports.colors = ['#0000CC', '#0000FF', '#0033CC', '#0033FF', '#0066CC', '#0066FF', '#0099CC', '#0099FF', '#00CC00', '#00CC33', '#00CC66', '#00CC99', '#00CCCC', '#00CCFF', '#3300CC', '#3300FF', '#3333CC', '#3333FF', '#3366CC', '#3366FF', '#3399CC', '#3399FF', '#33CC00', '#33CC33', '#33CC66', '#33CC99', '#33CCCC', '#33CCFF', '#6600CC', '#6600FF', '#6633CC', '#6633FF', '#66CC00', '#66CC33', '#9900CC', '#9900FF', '#9933CC', '#9933FF', '#99CC00', '#99CC33', '#CC0000', '#CC0033', '#CC0066', '#CC0099', '#CC00CC', '#CC00FF', '#CC3300', '#CC3333', '#CC3366', '#CC3399', '#CC33CC', '#CC33FF', '#CC6600', '#CC6633', '#CC9900', '#CC9933', '#CCCC00', '#CCCC33', '#FF0000', '#FF0033', '#FF0066', '#FF0099', '#FF00CC', '#FF00FF', '#FF3300', '#FF3333', '#FF3366', '#FF3399', '#FF33CC', '#FF33FF', '#FF6600', '#FF6633', '#FF9900', '#FF9933', '#FFCC00', '#FFCC33'];
      /**
       * Currently only WebKit-based Web Inspectors, Firefox >= v31,
       * and the Firebug extension (any Firefox version) are known
       * to support "%c" CSS customizations.
       *
       * TODO: add a `localStorage` variable to explicitly enable/disable colors
       */
      // eslint-disable-next-line complexity

      function useColors() {
        // NB: In an Electron preload script, document will be defined but not fully
        // initialized. Since we know we're in Chrome, we'll just detect this case
        // explicitly
        if (typeof window !== 'undefined' && window.process && (window.process.type === 'renderer' || window.process.__nwjs)) {
          return true;
        } // Internet Explorer and Edge do not support colors.


        if (typeof navigator !== 'undefined' && navigator.userAgent && navigator.userAgent.toLowerCase().match(/(edge|trident)\/(\d+)/)) {
          return false;
        } // Is webkit? http://stackoverflow.com/a/16459606/376773
        // document is undefined in react-native: https://github.com/facebook/react-native/pull/1632


        return typeof document !== 'undefined' && document.documentElement && document.documentElement.style && document.documentElement.style.WebkitAppearance || // Is firebug? http://stackoverflow.com/a/398120/376773
        typeof window !== 'undefined' && window.console && (window.console.firebug || window.console.exception && window.console.table) || // Is firefox >= v31?
        // https://developer.mozilla.org/en-US/docs/Tools/Web_Console#Styling_messages
        typeof navigator !== 'undefined' && navigator.userAgent && navigator.userAgent.toLowerCase().match(/firefox\/(\d+)/) && parseInt(RegExp.$1, 10) >= 31 || // Double check webkit in userAgent just in case we are in a worker
        typeof navigator !== 'undefined' && navigator.userAgent && navigator.userAgent.toLowerCase().match(/applewebkit\/(\d+)/);
      }
      /**
       * Colorize log arguments if enabled.
       *
       * @api public
       */


      function formatArgs(args) {
        args[0] = (this.useColors ? '%c' : '') + this.namespace + (this.useColors ? ' %c' : ' ') + args[0] + (this.useColors ? '%c ' : ' ') + '+' + module.exports.humanize(this.diff);

        if (!this.useColors) {
          return;
        }

        var c = 'color: ' + this.color;
        args.splice(1, 0, c, 'color: inherit'); // The final "%c" is somewhat tricky, because there could be other
        // arguments passed either before or after the %c, so we need to
        // figure out the correct index to insert the CSS into

        var index = 0;
        var lastC = 0;
        args[0].replace(/%[a-zA-Z%]/g, function (match) {
          if (match === '%%') {
            return;
          }

          index++;

          if (match === '%c') {
            // We only are interested in the *last* %c
            // (the user may have provided their own)
            lastC = index;
          }
        });
        args.splice(lastC, 0, c);
      }
      /**
       * Invokes `console.log()` when available.
       * No-op when `console.log` is not a "function".
       *
       * @api public
       */


      function log() {
        var _console;

        // This hackery is required for IE8/9, where
        // the `console.log` function doesn't have 'apply'
        return (typeof console === "undefined" ? "undefined" : _typeof(console)) === 'object' && console.log && (_console = console).log.apply(_console, arguments);
      }
      /**
       * Save `namespaces`.
       *
       * @param {String} namespaces
       * @api private
       */


      function save(namespaces) {
        try {
          if (namespaces) {
            exports.storage.setItem('debug', namespaces);
          } else {
            exports.storage.removeItem('debug');
          }
        } catch (error) {// Swallow
          // XXX (@Qix-) should we be logging these?
        }
      }
      /**
       * Load `namespaces`.
       *
       * @return {String} returns the previously persisted debug modes
       * @api private
       */


      function load() {
        var r;

        try {
          r = exports.storage.getItem('debug');
        } catch (error) {} // Swallow
        // XXX (@Qix-) should we be logging these?
        // If debug isn't set in LS, and we're in Electron, try to load $DEBUG


        if (!r && typeof process !== 'undefined' && 'env' in process) {
          r = undefined;
        }

        return r;
      }
      /**
       * Localstorage attempts to return the localstorage.
       *
       * This is necessary because safari throws
       * when a user disables cookies/localstorage
       * and you attempt to access it.
       *
       * @return {LocalStorage}
       * @api private
       */


      function localstorage() {
        try {
          // TVMLKit (Apple TV JS Runtime) does not have a window object, just localStorage in the global context
          // The Browser also has localStorage in the global context.
          return localStorage;
        } catch (error) {// Swallow
          // XXX (@Qix-) should we be logging these?
        }
      }

      module.exports = require('./common')(exports);
      var formatters = module.exports.formatters;
      /**
       * Map %j to `JSON.stringify()`, since no Web Inspectors do that by default.
       */

      formatters.j = function (v) {
        try {
          return JSON.stringify(v);
        } catch (error) {
          return '[UnexpectedJSONParseError]: ' + error.message;
        }
      };
    }).call(this, require('_process'));
  }, {
    "./common": 42,
    "_process": 43
  }],
  42: [function (require, module, exports) {
    /**
     * This is the common logic for both the Node.js and web browser
     * implementations of `debug()`.
     */
    function setup(env) {
      createDebug.debug = createDebug;
      createDebug.default = createDebug;
      createDebug.coerce = coerce;
      createDebug.disable = disable;
      createDebug.enable = enable;
      createDebug.enabled = enabled;
      createDebug.humanize = require('ms');
      Object.keys(env).forEach(function (key) {
        createDebug[key] = env[key];
      });
      /**
      * Active `debug` instances.
      */

      createDebug.instances = [];
      /**
      * The currently active debug mode names, and names to skip.
      */

      createDebug.names = [];
      createDebug.skips = [];
      /**
      * Map of special "%n" handling functions, for the debug "format" argument.
      *
      * Valid key names are a single, lower or upper-case letter, i.e. "n" and "N".
      */

      createDebug.formatters = {};
      /**
      * Selects a color for a debug namespace
      * @param {String} namespace The namespace string for the for the debug instance to be colored
      * @return {Number|String} An ANSI color code for the given namespace
      * @api private
      */

      function selectColor(namespace) {
        var hash = 0;

        for (var i = 0; i < namespace.length; i++) {
          hash = (hash << 5) - hash + namespace.charCodeAt(i);
          hash |= 0; // Convert to 32bit integer
        }

        return createDebug.colors[Math.abs(hash) % createDebug.colors.length];
      }

      createDebug.selectColor = selectColor;
      /**
      * Create a debugger with the given `namespace`.
      *
      * @param {String} namespace
      * @return {Function}
      * @api public
      */

      function createDebug(namespace) {
        var prevTime;

        function debug() {
          for (var _len6 = arguments.length, args = new Array(_len6), _key6 = 0; _key6 < _len6; _key6++) {
            args[_key6] = arguments[_key6];
          }

          // Disabled?
          if (!debug.enabled) {
            return;
          }

          var self = debug; // Set `diff` timestamp

          var curr = Number(new Date());
          var ms = curr - (prevTime || curr);
          self.diff = ms;
          self.prev = prevTime;
          self.curr = curr;
          prevTime = curr;
          args[0] = createDebug.coerce(args[0]);

          if (typeof args[0] !== 'string') {
            // Anything else let's inspect with %O
            args.unshift('%O');
          } // Apply any `formatters` transformations


          var index = 0;
          args[0] = args[0].replace(/%([a-zA-Z%])/g, function (match, format) {
            // If we encounter an escaped % then don't increase the array index
            if (match === '%%') {
              return match;
            }

            index++;
            var formatter = createDebug.formatters[format];

            if (typeof formatter === 'function') {
              var val = args[index];
              match = formatter.call(self, val); // Now we need to remove `args[index]` since it's inlined in the `format`

              args.splice(index, 1);
              index--;
            }

            return match;
          }); // Apply env-specific formatting (colors, etc.)

          createDebug.formatArgs.call(self, args);
          var logFn = self.log || createDebug.log;
          logFn.apply(self, args);
        }

        debug.namespace = namespace;
        debug.enabled = createDebug.enabled(namespace);
        debug.useColors = createDebug.useColors();
        debug.color = selectColor(namespace);
        debug.destroy = destroy;
        debug.extend = extend; // Debug.formatArgs = formatArgs;
        // debug.rawLog = rawLog;
        // env-specific initialization logic for debug instances

        if (typeof createDebug.init === 'function') {
          createDebug.init(debug);
        }

        createDebug.instances.push(debug);
        return debug;
      }

      function destroy() {
        var index = createDebug.instances.indexOf(this);

        if (index !== -1) {
          createDebug.instances.splice(index, 1);
          return true;
        }

        return false;
      }

      function extend(namespace, delimiter) {
        var newDebug = createDebug(this.namespace + (typeof delimiter === 'undefined' ? ':' : delimiter) + namespace);
        newDebug.log = this.log;
        return newDebug;
      }
      /**
      * Enables a debug mode by namespaces. This can include modes
      * separated by a colon and wildcards.
      *
      * @param {String} namespaces
      * @api public
      */


      function enable(namespaces) {
        createDebug.save(namespaces);
        createDebug.names = [];
        createDebug.skips = [];
        var i;
        var split = (typeof namespaces === 'string' ? namespaces : '').split(/[\s,]+/);
        var len = split.length;

        for (i = 0; i < len; i++) {
          if (!split[i]) {
            // ignore empty strings
            continue;
          }

          namespaces = split[i].replace(/\*/g, '.*?');

          if (namespaces[0] === '-') {
            createDebug.skips.push(new RegExp('^' + namespaces.substr(1) + '$'));
          } else {
            createDebug.names.push(new RegExp('^' + namespaces + '$'));
          }
        }

        for (i = 0; i < createDebug.instances.length; i++) {
          var instance = createDebug.instances[i];
          instance.enabled = createDebug.enabled(instance.namespace);
        }
      }
      /**
      * Disable debug output.
      *
      * @return {String} namespaces
      * @api public
      */


      function disable() {
        var namespaces = [].concat(_toConsumableArray(createDebug.names.map(toNamespace)), _toConsumableArray(createDebug.skips.map(toNamespace).map(function (namespace) {
          return '-' + namespace;
        }))).join(',');
        createDebug.enable('');
        return namespaces;
      }
      /**
      * Returns true if the given mode name is enabled, false otherwise.
      *
      * @param {String} name
      * @return {Boolean}
      * @api public
      */


      function enabled(name) {
        if (name[name.length - 1] === '*') {
          return true;
        }

        var i;
        var len;

        for (i = 0, len = createDebug.skips.length; i < len; i++) {
          if (createDebug.skips[i].test(name)) {
            return false;
          }
        }

        for (i = 0, len = createDebug.names.length; i < len; i++) {
          if (createDebug.names[i].test(name)) {
            return true;
          }
        }

        return false;
      }
      /**
      * Convert regexp to namespace
      *
      * @param {RegExp} regxep
      * @return {String} namespace
      * @api private
      */


      function toNamespace(regexp) {
        return regexp.toString().substring(2, regexp.toString().length - 2).replace(/\.\*\?$/, '*');
      }
      /**
      * Coerce `val`.
      *
      * @param {Mixed} val
      * @return {Mixed}
      * @api private
      */


      function coerce(val) {
        if (val instanceof Error) {
          return val.stack || val.message;
        }

        return val;
      }

      createDebug.enable(createDebug.load());
      return createDebug;
    }

    module.exports = setup;
  }, {
    "ms": 3
  }],
  43: [function (require, module, exports) {
    // shim for using process in browser
    var process = module.exports = {}; // cached from whatever global is present so that test runners that stub it
    // don't break things.  But we need to wrap it in a try catch in case it is
    // wrapped in strict mode code which doesn't define any globals.  It's inside a
    // function because try/catches deoptimize in certain engines.

    var cachedSetTimeout;
    var cachedClearTimeout;

    function defaultSetTimout() {
      throw new Error('setTimeout has not been defined');
    }

    function defaultClearTimeout() {
      throw new Error('clearTimeout has not been defined');
    }

    (function () {
      try {
        if (typeof setTimeout === 'function') {
          cachedSetTimeout = setTimeout;
        } else {
          cachedSetTimeout = defaultSetTimout;
        }
      } catch (e) {
        cachedSetTimeout = defaultSetTimout;
      }

      try {
        if (typeof clearTimeout === 'function') {
          cachedClearTimeout = clearTimeout;
        } else {
          cachedClearTimeout = defaultClearTimeout;
        }
      } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
      }
    })();

    function runTimeout(fun) {
      if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
      } // if setTimeout wasn't available but was latter defined


      if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
      }

      try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
      } catch (e) {
        try {
          // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
          return cachedSetTimeout.call(null, fun, 0);
        } catch (e) {
          // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
          return cachedSetTimeout.call(this, fun, 0);
        }
      }
    }

    function runClearTimeout(marker) {
      if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
      } // if clearTimeout wasn't available but was latter defined


      if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
      }

      try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
      } catch (e) {
        try {
          // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
          return cachedClearTimeout.call(null, marker);
        } catch (e) {
          // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
          // Some versions of I.E. have different rules for clearTimeout vs setTimeout
          return cachedClearTimeout.call(this, marker);
        }
      }
    }

    var queue = [];
    var draining = false;
    var currentQueue;
    var queueIndex = -1;

    function cleanUpNextTick() {
      if (!draining || !currentQueue) {
        return;
      }

      draining = false;

      if (currentQueue.length) {
        queue = currentQueue.concat(queue);
      } else {
        queueIndex = -1;
      }

      if (queue.length) {
        drainQueue();
      }
    }

    function drainQueue() {
      if (draining) {
        return;
      }

      var timeout = runTimeout(cleanUpNextTick);
      draining = true;
      var len = queue.length;

      while (len) {
        currentQueue = queue;
        queue = [];

        while (++queueIndex < len) {
          if (currentQueue) {
            currentQueue[queueIndex].run();
          }
        }

        queueIndex = -1;
        len = queue.length;
      }

      currentQueue = null;
      draining = false;
      runClearTimeout(timeout);
    }

    process.nextTick = function (fun) {
      var args = new Array(arguments.length - 1);

      if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
          args[i - 1] = arguments[i];
        }
      }

      queue.push(new Item(fun, args));

      if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
      }
    }; // v8 likes predictible objects


    function Item(fun, array) {
      this.fun = fun;
      this.array = array;
    }

    Item.prototype.run = function () {
      this.fun.apply(null, this.array);
    };

    process.title = 'browser';
    process.env = {};
    process.argv = [];
    process.version = ''; // empty string to avoid regexp issues

    process.versions = {};

    function noop() {}

    process.on = noop;
    process.addListener = noop;
    process.once = noop;
    process.off = noop;
    process.removeListener = noop;
    process.removeAllListeners = noop;
    process.emit = noop;
    process.prependListener = noop;
    process.prependOnceListener = noop;

    process.listeners = function (name) {
      return [];
    };

    process.binding = function (name) {
      throw new Error('process.binding is not supported');
    };

    process.cwd = function () {
      return '/';
    };

    process.chdir = function (dir) {
      throw new Error('process.chdir is not supported');
    };

    process.umask = function () {
      return 0;
    };
  }, {}]
}, {}, [1]);
},{"process":"../../../../../../../usr/local/lib/node_modules/parcel/node_modules/process/browser.js"}],"../../../../../../../usr/local/lib/node_modules/parcel/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "61865" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else if (location.reload) {
        // `location` global exists in a web worker context but lacks `.reload()` function.
        location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["../../../../../../../usr/local/lib/node_modules/parcel/src/builtins/hmr-runtime.js","scripts/threads-bundle.js"], null)
//# sourceMappingURL=/threads-bundle.c3e946fb.js.map