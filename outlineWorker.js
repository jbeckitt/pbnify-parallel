function neighborsSame(mat, x, y) {
	if (mat.length < 1) {
		return false;
	}
    var width = mat[0].length;
    var height = mat.length;
    var val = mat[x][y];
    // var xRel = [-1, 0, 1, -1, 0, 1, -1, 0, 1];
    // var yRel = [-1, -1, -1, 0, 0, 0, 1, 1, 1];
    // var xRel = [0, -1, 1, 0];
    // var yRel = [-1, 0, 0, 1];
    var xRel = [1, 0];
    var yRel = [0, 1];
    for (var i = 0; i < xRel.length; i++) {
  var xx = x + xRel[i];
  var yy = y + yRel[i];
  if (xx >= 0 && xx < width && yy >= 0 && yy < height) {
      if (mat[yy][xx] != val) {
    return false;
      }
  }
    }
    return true;
};

self.onmessage = function(event) {
	var wp = event.data;
	
	wp.result = neighborsSame(wp.section, 1, 1) ? 0 : 1;
	self.postMessage(wp);
	self.close();
}